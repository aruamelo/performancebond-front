// assets
import { IconUsers, IconBuildingFactory2, IconUsersGroup } from '@tabler/icons-react'

// constant
const icons = { IconUsers, IconBuildingFactory2, IconUsersGroup }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const recursoshumanos = {
  id: 'recursoshumanos',
  title: 'Recursos Humanos',
  type: 'group',
  permission: 'recursos_humanos__visualizar',
  children: [
    {
      id: 'recursoshumanos',
      title: 'Recursos Humanos',
      type: 'collapse',
      url: '/recursoshumanos',
      icon: icons.IconUsers,

      children: [
        {
          id: 'relacaodepessoal',
          title: 'Relação de Pessoal',
          type: 'item',
          url: '/pages/recursoshumanos/relacaodepessoal',
          permission: 'relacao_de_pessoal',
          icon: icons.IconUsers,
        },
        {
          id: 'empresa',
          title: 'Empresa',
          type: 'item',
          url: '/pages/recursoshumanos/empresa',
          permission: 'empresa',
          icon: icons.IconBuildingFactory2
        },
        {
          id: 'cargo',
          title: 'Cargo',
          type: 'item',
          url: '/pages/recursoshumanos/cargo',
          permission: 'cargo',
          icon: icons.IconUsersGroup
        },
        {
          id: 'setor',
          title: 'Setor',
          type: 'item',
          url: '/pages/recursoshumanos/setor',
          permission: 'setor',
          icon: icons.IconUsersGroup
        }
      ]

    }
  ]
}

export default recursoshumanos
