// assets
import { IconPremiumRights, IconTractor, IconUserDollar } from '@tabler/icons-react'
import medicaoconstrutora from "./medicaoconstrutora";
import medicaodocliente from "./medicaodocliente"
// constant
const icons = { IconPremiumRights, IconTractor, IconUserDollar }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const medicoes = {
    id: 'medicoes',
    title: 'Medição',
    type: 'group',
    permission: 'medicao_da_construtora__visualizar',
    children: [
        {
            id: 'medicoes',
            title: 'Medição',
            type: 'collapse',
            icon: icons.IconPremiumRights,
            children: [
                {
                    id: 'medicaoconstrutora',
                    title: 'Construtora',
                    type: 'collapse',
                    permission: 'medicao_da_construtora__visualizar',
                    icon: icons.IconTractor,
                    children: medicaoconstrutora
                },
                {
                    id: 'medicaocliente',
                    title: 'Cliente',
                    type: 'collapse',
                    permission: 'medicao_do_cliente__visualizar',
                    icon: icons.IconUserDollar,
                    children: medicaodocliente
                }
            ]

        }
    ]
}

export default medicoes
