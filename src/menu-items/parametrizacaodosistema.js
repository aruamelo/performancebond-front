// assets
import { IconSettingsAutomation, IconSitemap } from '@tabler/icons-react'

// constant
const icons = { IconSettingsAutomation, IconSitemap }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const parametrizacaodosistema = {
  id: 'parametrizacaodosistema',
  title: 'Parametrização do sistema',
  type: 'group',
  permission: 'parametrizacao_do_sistema__visualizar',
  children: [
    {
      id: 'parametrizacaodosistema',
      title: 'Parametrização do sistema',
      type: 'collapse',
      url: '/parametrizacaodosistema',
      icon: icons.IconSettingsAutomation,

      children: [
        {
          id: 'codificacaodedocumento',
          title: 'Codificação de documento',
          type: 'collapse',
          url: '/pages/parametrizacaodosistema/codificacaodedocumento',
          permission: 'parametrizacao_do_sistema__codificacao_de_documento',
          icon: icons.IconSettingsAutomation,
          children: [
            {
              id: 'codigodocumento',
              title: 'Código do documento',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/codigodocumento/index',
              permission: 'codificacao_de_documento__codigo_do_documento',
              icon: icons.IconSitemap,
            },
            {
              id: 'frentedeservico',
              title: 'Frente de serviço',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/frentedeservico/index',
              permission: 'frente_de_servico__codificacao_de_documento__frente_de_servico',
              icon: icons.IconSitemap,
            },
            {
              id: 'trechos',
              title: 'Trechos',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/trechos/index',
              permission: 'codificacao_de_documento__trechos',
              icon: icons.IconSitemap,
            },
            {
              id: 'nucleo',
              title: 'Núcleo',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/nucleo/index',
              permission: 'codificacao_de_documento__nucleos',
              icon: icons.IconSitemap,
            },
            {
              id: 'fasedoprojeto',
              title: 'Fase do projeto',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/fasedoprojeto/index',
              permission: 'codificacao_de_documento__fases_do_projeto',
              icon: icons.IconSitemap,
            },
            {
              id: 'disciplinas',
              title: 'Disciplinas',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/disciplinas/index',
              permission: 'codificacao_de_documento__fases_do_projeto',
              icon: icons.IconSitemap,
            },
            {
              id: 'tiposdedocumento',
              title: 'Tipo de documento',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/tiposdedocumento/index',
              permission: 'codificacao_de_documento__tipos_de_documento',
              icon: icons.IconSitemap,
            },
            {
              id: 'subcontratos',
              title: 'Subcontratos',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/subcontratos/index',
              permission: 'codificacao_de_documento__subcontratos',
              icon: icons.IconSitemap,
             },
            {
              id: 'parametrizacaocodificacaodedocumento',
              title: 'Parametrizar codificação de documento',
              type: 'item',
              url: '/pages/parametrizacaodosistema/codificacaodedocumento/parametrizacaocodificacaodedocumento/index',
              permission: 'codificacao_de_documento__parametrizar_codificacao_de_documento',
              icon: icons.IconSitemap,
             }
          ]
        },
        // {
        //   id: 'colaborador',
        //   title: 'Colaborador',
        //   type: 'item',
        //   url: '/pages/parametrizacaodosistema/colaborador',
        //   permission: 'parametrizacao_do_sistema__colaborador',
        //   icon: icons.IconSettingsAutomation,
        // }
      ]

    }
  ]
}

export default parametrizacaodosistema
