// assets
import { IconReportMoney, IconFilePencil, IconCash, IconBrandCashapp, IconSettingsAutomation }
    from '@tabler/icons-react'

// constant
const icons = { IconReportMoney, IconFilePencil, IconCash, IconBrandCashapp, IconSettingsAutomation }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const medicaoconstrutora = [
        {
          id: 'folhaderosto',
          title: 'Folha de Rosto',
          type: 'item',
          url: '/pages/medicaoconstrutora/folhaderosto',
          permission: 'folha_de_rosto',
          icon: icons.IconReportMoney
        },
        {
          id: 'boletimmedicao',
          title: 'Boletim de Medição',
          type: 'item',
          url: '/pages/medicaoconstrutora/boletimmedicao',
          permission: 'boletim_de_medicao_construtora',
          icon: icons.IconCash
        },
        {
          id: 'itemdocontrato',
          title: 'Item do Contrato',
          type: 'item',
          url: '/pages/medicaoconstrutora/itemdocontrato',
          permission: 'item_do_contrato',
          icon: icons.IconFilePencil
        },
        {
          id: 'indicedereajuste',
          title: 'Índice de Reajuste',
          type: 'item',
          url: '/pages/medicaoconstrutora/indicedereajuste',
          permission: 'Indice_de_reajuste_construtora',
          icon: icons.IconBrandCashapp
        },
        {
          id: 'parametrizarvalordecontigencia',
          title: 'Parametrizar Valor de Contingência',
          type: 'item',
          url: '/pages/medicaoconstrutora/parametrizarvalordecontigencia',
          permission: 'parametrizar_valor_de_contingencia',
          icon: icons.IconSettingsAutomation
        }
      ]
export default medicaoconstrutora
