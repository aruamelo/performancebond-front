// assets
import {
  IconBrandTrello,
  IconChartInfographic,
  IconDashboard,
  IconEmergencyBed,
  IconTimelineEvent
} from '@tabler/icons-react'

// constant
const icons = { IconTimelineEvent, IconBrandTrello, IconDashboard, IconEmergencyBed, IconChartInfographic }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const planejamentos = {
  id: 'planejamentos',
  title: 'Planejamento',
  type: 'group',
  permission: 'planejamento__visualizar',
  children: [
    {
      id: 'administrativo',
      title: 'Planejamento',
      type: 'collapse',
      url: '/planejamentos',
      icon: icons.IconTimelineEvent,

      children: [
        {
          id: 'cronograma',
          title: 'Cronograma',
          type: 'item',
          url: '/pages/planejamentos/cronograma',
          permission: 'cronograma',
          icon: icons.IconTimelineEvent,
        },
        {
          id: 'quadrodeatividade',
          title: 'Quadro de Atividade',
          type: 'item',
          url: '/pages/planejamentos/quadrodeatividade',
          permission: 'quadro_de_atividade',
          icon: icons.IconBrandTrello
        },
        {
          id: 'avancofisicofinanceiro',
          title: 'Avanço Físico Financeiro',
          type: 'item',
          url: '/pages/planejamentos/avancofisicofinanceiro',
          permission: 'avanco_fisico_financeiro',
          icon: icons.IconDashboard
        },
        {
          id: 'acidentedetrabalho',
          title: 'Acidente De Trabalho',
          type: 'item',
          url: '/pages/planejamentos/acidentedetrabalho',
          permission: 'acidente_de_trabalho',
          icon: icons.IconEmergencyBed
        },
        {
          id: 'opr',
          title: 'One Page Report',
          type: 'item',
          url: '/pages/planejamentos/opr',
          permission: 'opr',
          icon: icons.IconChartInfographic
        }
      ]

    }
  ]
}

export default planejamentos
