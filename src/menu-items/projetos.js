// assets
import { IconFiles, IconListCheck, IconFileCertificate, IconCheck, IconChecks } from '@tabler/icons-react'

// constant
const icons = { IconFiles, IconListCheck, IconFileCertificate, IconCheck, IconChecks }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const projetos = {
  id: 'projetos',
  title: 'Projetos',
  type: 'group',
  permission: 'projetos__visualizar',
  children: [
    {
      id: 'projetos',
      title: 'Projetos',
      type: 'collapse',
      url: '/projetos',
      icon: icons.IconFiles,

      children: [
        {
          id: 'listagemdedocumento',
          title: 'Listagem de Documentos',
          type: 'item',
          url: '/pages/projetos/listagemdedocumento',
          permission: 'listagem_de_documentos',
          icon: icons.IconFiles,
        },
        {
          id: 'analisededocumento',
          title: 'Análisar Documento',
          type: 'collapse',
          url: '/pages/projetos/analisededocumento',
          permission: 'analise_de_documento',
          icon: icons.IconListCheck,
          children: [
            {
              id: 'analisededocumento_verificar',
              title: 'Verificar documento',
              type: 'item',
              url: '/pages/projetos/verificar',
              permission: 'remessa_de_arquivo__analisar_remessa_submetida__verificador',
              icon: icons.IconCheck,
            },
            {
              id: 'analisededocumento_aprovar',
              title: 'Aprovar documento',
              type: 'item',
              url: '/pages/projetos/aprovar',
              permission: 'remessa_de_arquivo__analisar_remessa_submetida__aprovador',
              icon: icons.IconChecks,
            },
          ]
        },
        // {
        //   id: 'responsabilidadetecnica',
        //   title: 'Responsabilidade Técnica',
        //   type: 'item',
        //   url: '/pages/projetos/responsabilidadetecnica',
        //   permission: 'responsabilidade_tecnica',
        //   icon: icons.IconFileCertificate
        // },
        // {
        //   id: 'guiaderemessadedocumento',
        //   title: 'Guia de Remessa de Documento',
        //   type: 'item',
        //   url: '/pages/projetos/guiaderemessadedocumento',
        //   permission: 'guia_de_remessa_de_documento',
        //   icon: icons.IconFileCertificate
        // },
        // {
        //   id: 'complementoalteracaoprojeto',
        //   title: 'Complemento Alteração Projeto',
        //   type: 'item',
        //   url: '/pages/projetos/complementoalteracaoprojeto',
        //   permission: 'complemento_alteracao_de_projeto',
        //   icon: icons.IconFileCertificate
        // }
      ]

    }
  ]
}

export default projetos
