// assets
import { IconUserPlus, IconUsersGroup, IconLicense } from '@tabler/icons-react'

// constant
const icons = { IconUserPlus, IconUsersGroup, IconLicense }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const permissoes = {
  id: 'permissoes',
  title: 'Permissões',
  type: 'group',
  permission: 'permissoes__visualizar',
  children: [
    {
      id: 'permissoes',
      title: 'Permissões',
      type: 'collapse',
      url: '/permissoes',
      icon: icons.IconUserPlus,

      children: [
        {
          id: 'colaborador',
          title: 'Colaborador',
          type: 'item',
          url: '/pages/permissoes/colaborador/index',
          permission: 'permissoes__colaborado/index',
          icon: icons.IconUserPlus,
        },
        {
          id: 'usuariodosistema',
          title: 'Usuário do Sistema',
          type: 'item',
          url: '/pages/permissoes/usuariodosistema/index',
          permission: 'usuario_do_sistema',
          icon: icons.IconUserPlus,
        },
        {
          id: 'funcionalidades',
          title: 'Funcionalidades',
          type: 'item',
          url: '/pages/permissoes/funcionalidades/index',
          permission: 'funcionalidades',
          icon: icons.IconLicense,
        },
        {
          id: 'grupodeacesso',
          title: 'Grupo de Acesso',
          type: 'item',
          url: '/pages/permissoes/grupodeacesso/index',
          permission: 'grupo_de_acesso',
          icon: icons.IconUsersGroup
        }
      ]

    }
  ]
}

export default permissoes
