// assets
import { IconCash, IconUsersGroup, IconFile,
    IconUserDollar, IconSettingsAutomation, IconBrandCashapp, IconCoins,
    IconBusinessplan } from '@tabler/icons-react'

// constant
const icons = { IconCash, IconUsersGroup, IconFile,
    IconUserDollar, IconSettingsAutomation, IconBrandCashapp, IconCoins,
    IconBusinessplan }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const medicaodocliente = [
        {
          id: 'boletimdemedicao',
          title: 'Boletim de Medição',
          type: 'item',
          url: '/pages/medicaodocliente/boletimdemedicao',
          permission: 'boletim_de_medicao_cliente',
          icon: icons.IconCash
        },
        {
          id: 'grupodetrabalho',
          title: 'Grupo de Trabalho',
          type: 'item',
          url: '/pages/medicaodocliente/grupodetrabalho',
          permission: 'grupo_de_trabalho',
          icon: icons.IconUsersGroup
        },
        {
          id: 'titulodadespesareembolsavel',
          title: 'Título da Despesa Reembolsável',
          type: 'item',
          url: '/pages/medicaodocliente/titulodadespesareembolsavel',
          permission: 'titulo_da_despesa_reembolsavel',
          icon: icons.IconFile
        },
        {
          id: 'remuneracao',
          title: 'Remuneração',
          type: 'item',
          url: '/pages/medicaodocliente/remuneracao',
          permission: 'remuneracao',
          icon: icons.IconUserDollar
        },
        {
          id: 'parametrizacao',
          title: 'Parametrização',
          type: 'item',
          url: '/pages/medicaodocliente/parametrizacao',
          permission: 'parametrizacao',
          icon: icons.IconSettingsAutomation
        },
        {
          id: 'rateio',
          title: 'Rateio',
          type: 'item',
          url: '/pages/medicaodocliente/rateio',
          permission: 'rateio',
          icon: icons.IconBusinessplan
        },
        {
          id: 'conversaodemoeda',
          title: 'Conversão de Moeda',
          type: 'item',
          url: '/pages/medicaodocliente/conversaodemoeda',
          permission: 'conversao_de_moeda',
          icon: icons.IconBusinessplan
        },
        {
          id: 'marcadordemoeda',
          title: 'Marcador de Moeda',
          type: 'item',
          url: '/pages/medicaodocliente/marcadordemoeda',
          permission: 'marcador_de_moeda',
          icon: icons.IconCoins
        },
        {
          id: 'indicedereajuste',
          title: 'Índice de Reajuste',
          type: 'item',
          url: '/pages/medicaodocliente/indicedereajuste',
          permission: 'Indice_de_reajuste_cliente',
          icon: icons.IconBrandCashapp
        },
        {
          id: 'prestacaodecontas',
          title: 'Prestação de Contas',
          type: 'item',
          url: '/pages/medicaodocliente/prestacaodecontas',
          permission: 'prestacao_de_contas',
          icon: icons.IconBusinessplan
        }
  ]

export default medicaodocliente
