// assets
import { IconTractor, IconCameraSelfie, IconMessageReport,
  IconFileDescription, IconHammer, IconSettingsAutomation, IconBrandHipchat, IconMedal2, IconFileTime } from '@tabler/icons-react'

// constant
const icons = { IconTractor, IconCameraSelfie, IconMessageReport,
  IconFileDescription, IconHammer, IconSettingsAutomation, IconBrandHipchat, IconMedal2, IconFileTime }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const frentedeservico = {
  id: 'frentedeservico',
  title: 'Frente de Serviço',
  type: 'group',
  permission: 'frente_de_servico__visualizar',
  children: [
    {
      id: 'frentedeservico',
      title: 'Frente de Serviço',
      type: 'collapse',
      url: '/frentedeservico',
      icon: icons.IconTractor,

      children: [
        {
          id: 'diariodeobra',
          title: 'Diário de Obra',
          type: 'item',
          url: '/pages/frentedeservico/diariodeobra',
          permission: 'diario_de_obra',
          icon: icons.IconTractor
        },
        {
          id: 'diariodeatividade',
          title: 'Diário de Atividade',
          type: 'item',
          url: '/pages/frentedeservico/diariodeatividade',
          permission: 'diario_de_atividade',
          icon: icons.IconTractor
        },
        {
          id: 'relatoriofotografico',
          title: 'Relatório Fotográfico',
          type: 'item',
          url: '/pages/frentedeservico/relatoriofotografico',
          permission: 'relatorio_fotografico',
          icon: icons.IconCameraSelfie
        },
        {
          id: 'pontodeatencao',
          title: 'Ponto de Atenção',
          type: 'item',
          url: '/pages/frentedeservico/pontodeatencao',
          permission: 'ponto_de_atencao',
          icon: icons.IconMessageReport
        },
        {
          id: 'relatoriostatusdeatividade',
          title: 'Status de Atividade',
          type: 'item',
          url: '/pages/frentedeservico/relatoriostatusdeatividade',
          permission: 'relatorio_do_status_de_atividade',
          icon: icons.IconFileDescription
        },
        {
          id: 'documentotecnicodiverso',
          title: 'Documento Técnico',
          type: 'item',
          url: '/pages/frentedeservico/documentotecnicodiverso',
          permission: 'documento_tecnico_diverso',
          icon: icons.IconFileDescription
        },
        {
          id: 'liberacaodeservico',
          title: 'Liberação de Serviço',
          type: 'item',
          url: '/pages/frentedeservico/liberacaodeservico',
          permission: 'liberacao_de_servico',
          icon: icons.IconHammer
        },
        {
          id: 'interferencia',
          title: 'Interferência',
          type: 'item',
          url: '/pages/frentedeservico/interferencia',
          permission: 'interferencia',
          icon: icons.IconMessageReport
        },
        {
          id: 'licencaalvara',
          title: 'Licença e Alvará',
          type: 'item',
          url: '/pages/frentedeservico/licencaalvara',
          permission: 'licenca_e_alvara',
          icon: icons.IconFileTime
        },
        {
          id: 'comunicacaosocial',
          title: 'Comunicação Social',
          type: 'item',
          url: '/pages/frentedeservico/comunicacaosocial',
          permission: 'comunicacao_social',
          icon: icons.IconBrandHipchat
        },
        {
          id: 'certificacaoleed',
          title: 'Certificação Leed',
          type: 'item',
          url: '/pages/frentedeservico/certificacaoleed',
          permission: 'certificacao_leed',
          icon: icons.IconMedal2
        },
        {
          id: 'codificacaodedocumento',
          title: 'Codificação de Doc.',
          type: 'item',
          url: '/pages/frentedeservico/codificacaodedocumento',
          permission: 'frente_de_servico__codificacao_de_documento',
          icon: icons.IconSettingsAutomation
        }
      ]

    }
  ]
}

export default frentedeservico
