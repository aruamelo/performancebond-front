// assets
import { IconCloudUpload, IconUpload, IconBookUpload, IconFileTypePdf } from '@tabler/icons-react'

// constant
const icons = { IconCloudUpload, IconUpload, IconBookUpload, IconFileTypePdf }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const remessadearquivo = {
  id: 'remessadearquivo',
  title: 'Remessa de Arquivo',
  type: 'group',
  permission: 'remessa_de_arquivo__visualizar',
  children: [
    {
      id: 'remessadearquivo',
      title: 'Remessa de Arquivo',
      type: 'collapse',
      url: '/remessadearquivo',
      icon: icons.IconCloudUpload,

      children: [
        {
          id: 'entradadedocumento',
          title: 'Entrada de Documento',
          type: 'item',
          url: '/pages/remessadearquivo/entradadedocumento/index',
          permission: 'remessa_de_arquivo__submeter_arquivo',
          icon: icons.IconUpload
        },
        {
          id: 'documentosubmetido',
          title: 'Documento Submetido',
          type: 'item',
          url: '/pages/remessadearquivo/entradadedocumento/documentosubmetido',
          permission: 'documento_submetido',
          icon: icons.IconBookUpload
        }
        // ,
        // {
        //   id: 'eapdigital',
        //   title: 'EAP Digital',
        //   type: 'item',
        //   url: '/pages/remessadearquivo/eapdigital',
        //   permission: 'eap_digital',
        //   icon: icons.IconFileTypePdf
        // }
      ]

    }
  ]
}

export default remessadearquivo
