// assets
import { IconFileText, IconFileExport, IconFileArrowLeft, IconMail, IconMoodEdit } from '@tabler/icons-react'

// constant
const icons = {  IconFileText, IconFileExport, IconFileArrowLeft, IconMail, IconMoodEdit }
// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const administrativo = {
  id: 'administrativo',
  title: 'Administrativo',
  type: 'group',
  permission: 'administrativo__visualizar',
  children: [
    {
      id: 'administrativo',
      title: 'Administrativo',
      type: 'collapse',
      url: '/administrativo',
      icon: icons.IconFileText,
      children: [
        {
          id: 'cartas',
          title: 'Ofício enviado',
          type: 'collapse',
          permission: 'carta_enviada',
          icon: icons.IconMail,
          children: [
            {
              id: 'cartaenviada',
              title: 'Ofícios',
              type: 'item',
              url: '/pages/administrativo/oficioenviado/index',
              permission: 'carta_enviada',
              icon: icons.IconFileExport,

            },
            {
              id: 'cartarecebida',
              title: 'Máscara',
              type: 'item',
              url: '/pages/administrativo/mascaraoficioenviado/index',
              permission: 'administrativo__mascara_oficio_enviado',
              icon: icons.IconFileArrowLeft,
            }
          ]
        },
        // {
        //   id: 'atadereuniao',
        //   title: 'Ata de Reunião',
        //   type: 'item',
        //   url: '/pages/administrativo/atareuniao',
        //   permission: 'ata_de_reuniao',
        //   icon: icons.IconMoodEdit,
        // }
      ]

    }
  ]
}

export default administrativo
