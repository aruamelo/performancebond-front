import administrativo from './administrativo'
// import planejamento from './planejamento'
// import frentedeservico from './frentedeservico'
import projetos from './projetos'
// import recursoshumanos from './recursoshumanos'

import parametrizacaodosistema from './parametrizacaodosistema'

import permissoes from './permissoes'
import remessadearquivo from './remessadearquivo'
// import controledaqualidade from './controledaqualidade'
// import medicoes from "./medicoes";

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
  items: [
    administrativo,
    // medicoes,
    // planejamento,
    // frentedeservico,
    // controledaqualidade,
    // recursoshumanos,
    projetos,
    remessadearquivo,
    parametrizacaodosistema,
    permissoes
  ]
}

export default menuItems
