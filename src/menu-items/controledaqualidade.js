// assets
import { IconCheckupList, IconFileDatabase } from '@tabler/icons-react'

// constant
const icons = { IconCheckupList, IconFileDatabase }

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const controledaqualidade = {
  id: 'controledaqualidade',
  title: 'Controle da Qualidade',
  type: 'group',
  permission: 'controle_da_qualidade__visualizar',
  children: [
    {
      id: 'administrativo',
      title: 'Controle da Qualidade',
      type: 'collapse',
      url: '/controledaqualidade',
      icon: icons.IconCheckupList,

      children: [
        {
          id: 'liberacaodeterraplenagem',
          title: 'Liberação de Terraplenagem',
          type: 'item',
          url: '/pages/controledaqualidade/liberacaodeterraplenagem',
          permission: 'liberacao_de_terraplenagem',
          icon: icons.IconCheckupList
        },
        {
          id: 'liberacaodeconcretagem',
          title: 'Liberação de Concretagem',
          type: 'item',
          url: '/pages/controledaqualidade/liberacaodeconcretagem',
          permission: 'liberacao_de_concretagem',
          icon: icons.IconCheckupList
        },
        {
          id: 'altimetria',
          title: 'Altimetria',
          type: 'item',
          url: '/pages/controledaqualidade/altimetria',
          permission: 'altimetria',
          icon: icons.IconCheckupList
        },
        {
          id: 'planimetria',
          title: 'Planimetria',
          type: 'item',
          url: '/pages/controledaqualidade/planimetria',
          permission: 'planimetria',
          icon: icons.IconCheckupList
        },
        {
          id: 'naoconformidade',
          title: 'Não Conformidade',
          type: 'item',
          url: '/pages/controledaqualidade/naoconformidade',
          permission: 'nao_conformidade',
          icon: icons.IconCheckupList
        },
        {
          id: 'documentotecnologico',
          title: 'Documento Tecnológico',
          type: 'item',
          url: '/pages/controledaqualidade/documentotecnologico',
          permission: 'documento_tecnologico',
          icon: icons.IconFileDatabase
        },
        {
          id: 'relatoriodeconsultores',
          title: 'Relatório de Consultores',
          type: 'item',
          url: '/pages/controledaqualidade/relatoriodeconsultores',
          permission: 'relatorio_de_consultores',
          icon: icons.IconFileDatabase
        },
        {
          id: 'notafiscal',
          title: 'Nota Fiscal',
          type: 'item',
          url: '/pages/controledaqualidade/notafiscal',
          permission: 'nota_fiscal',
          icon: icons.IconFileDatabase
        }
      ]

    }
  ]
}

export default controledaqualidade
