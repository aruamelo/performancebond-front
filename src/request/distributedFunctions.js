import loginService from "../service/login/loginService";

const distributedFunctions = {
    /**
     * Método responsável redirecionar o usuário para a página de editar ou visualizar
     * @param {*} page
     * @param {*} id
     */
    redirectPage : (page, id = undefined) => {
        let pageRedirect = page
        //  `${pathname}/${page}`

        if (id !== undefined) {
        pageRedirect += `/${id}`
        }

        window.location.href = pageRedirect
    },
    pathProject: () => {
        let pathNameUrl = window.location.pathname
        pathNameUrl = pathNameUrl.slice(1, -1).split("/");
        pathNameUrl.pop();
        return pathNameUrl.join("/")
    },
    getIdContratoUser: () => {
        let user =  JSON.parse(localStorage.getItem('auth'))

        if (user == null) {
            window.location.href = '/pb'
        }
        return user.user.idContrato
    },
    handleErrorMessage: (exception = null) => {
        let responseErro = exception.response.data

        let mensagem = ''
        if (typeof responseErro === 'object') {
            Object.values(responseErro).map(erro => {
                mensagem += `${erro} <br>`
            })
        }

        if (mensagem === '') {
            mensagem = responseErro.message
        }

        return mensagem.charAt(0).toUpperCase() + mensagem.slice(1)
    },
    getDataUser: () => {
        let user =  JSON.parse(localStorage.getItem('auth'))

        if (user == null) {
            window.location.href = '/pb'
        }

        return user
    },
    permissionPage: (permission) => {
        let acessoPermitido =  loginService.validarPermissao(permission)

        // Verifica se o menu principal tem acesso
        if (!acessoPermitido) {
            window.location.href = '/pb/dashboard'
        }
    },
    isEmptyObject: (obj) =>{
    for (let key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            return false;
        }
    }
    return true;
    },
    viewArquivoDwfNavegador : (arquivoId) => {
        let urlProjeto =
            `/pb/pages/remessadearquivo/entradadedocumento/remessa_arquivo/visualizar_arquivo/${arquivoId}`;

        // Abrir a URL em uma nova guia
        window.open(urlProjeto, '_blank');
    }
}

export default distributedFunctions