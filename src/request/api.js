import axios from 'axios'
import config from "../config"

const api = axios.create(
  {
    baseURL: config.urlApi
  }
)

api.interceptors.request.use(
  config => {
    // Do something before request is sent
    const user = JSON.parse(localStorage.getItem('auth'))

    if (user != null) {
      config.headers["Authorization"] = "Bearer " + user.access_token;
    }

    return config;
  },
  error => {
    Promise.reject(error);
  }
);

export default api
