import api from './api'
import distributedFunctions from "./distributedFunctions";

const request = {
  post: async (namespace, data) => {
      return await api.post(`${namespace}`, data).catch(async (err) => {
        // Se for token inválido, envia para o login
        if (err.response.status === 401) {
          await refreshToken();
          return api(err.config);
          // window.location.href = '/pb'
        }

        return Promise.reject(err);
      })
  },
  put: async (namespace, data) => {
    return await api.put(`${namespace}`, data).catch(async (err) => {
      // Se for token inválido, envia para o login
      if (err.response.status === 401) {
        await refreshToken();
        return api(err.config);
        // window.location.href = '/pb'
      }

      return Promise.reject(err);
    })
  },
  get: async (namespace, data = {}) => {
    // Converter o JSON em objeto
    let dataArray = Object.entries(data)

    let queryString = '?'
    dataArray.forEach(([key, value]) => {
      queryString += `${key}=${value}&`
    })

    // Remover a última chave
    queryString = queryString.substring(0, queryString.length - 1)

    return await api.get(`${namespace}${queryString}`).then((response) => {
      return response
    }).catch(async (err) => {
      // Se for token inválido, envia para o login
      if (err.response.status === 500 && err.response.data.message == 'Token has expired') {
        await refreshToken();
        return api(err.config);
        // window.location.href = '/pb'
      }

      return Promise.reject(err);
    })
  },
  delete: async (id, namespace) => {
    const urlDelete = namespace ?? 'delete'
    let data = {id : id}
    return api.delete(urlDelete, { data }).catch(async (err) => {
      // Se for token inválido, envia para o login
      if (err.response.status === 401) {
        await refreshToken();
        return api(err.config);
        // window.location.href = '/pb'
      }

      return Promise.reject(err);
    })
  },
  postFile: async (namespace, formDataFile) => {
    await api.post(namespace, formDataFile, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }
    ).catch(async (err) => {
      // Se for token inválido, envia para o login
      if (err.response.status === 401) {
        await refreshToken();
        return api(err.config);
        // window.location.href = '/pb'
      }

      return Promise.reject(err);
    })
  },
  getFile: async(namespace, data) => {
    // Converter o JSON em objeto
    let dataArray = Object.entries(data)

    let queryString = '?'
    dataArray.forEach(([key, value]) => {
      queryString += `${key}=${value}&`
    })

    // Remover a última chave
    queryString = queryString.substring(0, queryString.length - 1)

    return await api.get(`${namespace}${queryString}`, {
      responseType: 'blob'
    }).then((response) => {
      return response
    }).catch(async (err) => {
      // Se for token inválido, envia para o login
      if (err.response.status === 401) {
        await refreshToken();
        return api(err.config);
        // window.location.href = '/pb'
      }

      return Promise.reject(err);
    })
  },
  login: async (namespace, data) => {
    return await api.post(`${namespace}`, data).catch(async (err) => {
      return Promise.reject(err);
    })
  }
}

async function refreshToken() {
  try {
    let user = distributedFunctions.getDataUser()

    const response = await api.post(
        'auth/refresh',
        {
          user_id: user.user.id,
          email: user.user.email,
          access_token: user.access_token
        },
        {
          headers: {
            'Authorization': `Bearer ${user.access_token}`
          }
        });
    const token = response.data.token;
    user.access_token = token
    // Atualize o token armazenado localmente (por exemplo, no localStorage)
    localStorage.setItem('auth', JSON.stringify(user));
  } catch (error) {
    if (error.code === 500 && error.response.data.message === 'The token has been blacklisted') {
      window.location.href = '/pb'
    }

    if (error.code === 'ERR_BAD_RESPONSE' && error.response.data.message === 'The token has been blacklisted') {
      window.location.href = '/pb'
    }

    return Promise.reject(error);
  }
}

export default request
