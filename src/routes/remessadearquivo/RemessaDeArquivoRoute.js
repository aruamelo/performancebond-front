import Loadable from "../../ui-component/Loadable";
import { lazy, React } from 'react'

const projetosView = {
    index : Loadable(lazy(() => import('../../views/pages/remessadearquivo/entradadedocumento'))),
    view : Loadable(lazy(() => import(`../../views/pages/remessadearquivo/remessaarquivo`))),
    visualizarArquivo : Loadable(lazy(() => import(`../../views/pages/remessadearquivo/visualizararquivo`))),
    documentoSubmetido : Loadable(lazy(() => import(`../../views/pages/remessadearquivo/documentosubmetido`))),
    visualizar_remessa_arquivo : Loadable(lazy(() => import(`../../views/pages/remessadearquivo/visualizarremessaarquivo`))),
}

const projetosViewRoute =
    [
        {
            path: 'index',
            element: <projetosView.index />,
            breadcrumbs: true
        },
        {
            path: 'documentosubmetido',
            element: <projetosView.documentoSubmetido />,
            breadcrumbs: true
        },
        {
            path: 'remessa_arquivo/:remessa_id',
            element: <projetosView.view />,
            breadcrumbs: true
        },
        {
            path: 'visualizar_remessa_arquivo/:remessa_id',
            element: <projetosView.visualizar_remessa_arquivo />,
            breadcrumbs: true
        },
        {
            path: 'remessa_arquivo/visualizar_arquivo/:arquivo_id',
            element: <projetosView.visualizarArquivo />,
            breadcrumbs: true
        }
    ]

export default projetosViewRoute