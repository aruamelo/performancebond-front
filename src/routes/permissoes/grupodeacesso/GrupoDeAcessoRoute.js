import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const grupoDeAcessoView = {
    index : Loadable(lazy(() => import('../../../views/pages/permissoes/grupodeacesso'))),
    new : Loadable(lazy(() => import('../../../views/pages/permissoes/grupodeacesso/new'))),
    view : Loadable(lazy(() => import('../../../views/pages/permissoes/grupodeacesso/view'))),
    edit : Loadable(lazy(() => import('../../../views/pages/permissoes/grupodeacesso/edit')))
}

const grupoDeAcessoViewRoute =
    [
        {
            path: 'index',
            element: <grupoDeAcessoView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <grupoDeAcessoView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <grupoDeAcessoView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <grupoDeAcessoView.new />,
            breadcrumbs: true
        }
    ]

export default grupoDeAcessoViewRoute