import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const funcionalidadeView = {
    index : Loadable(lazy(() => import('../../../views/pages/permissoes/funcionalidade'))),
    new : Loadable(lazy(() => import('../../../views/pages/permissoes/funcionalidade/new'))),
    view : Loadable(lazy(() => import('../../../views/pages/permissoes/funcionalidade/view'))),
    edit : Loadable(lazy(() => import('../../../views/pages/permissoes/funcionalidade/edit')))
}

const funcionalidadeViewRoute =
    [
        {
            path: 'index',
            element: <funcionalidadeView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <funcionalidadeView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <funcionalidadeView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <funcionalidadeView.new />,
            breadcrumbs: true
        }
    ]

export default funcionalidadeViewRoute