import Loadable from "../../ui-component/Loadable";
import { lazy, React } from 'react'

const remessadearquivoView = {
    index_verificar_projeto : Loadable(lazy(() => import('../../views/pages/projetos/indexverificarprojeto'))),
    visualizar_remessa_arquivo_verificador : Loadable(lazy(() => import(`../../views/pages/projetos/visualizarremessaarquivoverificador`))),
    analisar_remessa_documento_verificar : Loadable(lazy(() => import(`../../views/pages/projetos/remessaarquivoverificar`))),
    index_aprovar_projeto : Loadable(lazy(() => import(`../../views/pages/projetos/indexaprovarprojeto`))),
    visualizar_remessa_arquivo_aprovador : Loadable(lazy(() => import(`../../views/pages/projetos/visualizarremessaarquivoaprovador`))),
    analisar_remessa_documento_aprovador : Loadable(lazy(() => import(`../../views/pages/projetos/remessaarquivoaprovador`))),
    listagem_documentos : Loadable(lazy(() => import(`../../views/pages/projetos/listagemdedocumentos`))),
}

const remessadearquivoViewRoute =
    [
        {
            path: 'verificar',
            element: <remessadearquivoView.index_verificar_projeto />,
            breadcrumbs: true
        },
        {
            path: 'visualizar_remessa_arquivo/:remessa_id',
            element: <remessadearquivoView.visualizar_remessa_arquivo_verificador />,
            breadcrumbs: true
        },
        {
            path: 'aprovar',
            element: <remessadearquivoView.index_aprovar_projeto />,
            breadcrumbs: true
        },
        {
            path: 'visualizar_remessa_arquivo_aprovador/:remessa_id',
            element: <remessadearquivoView.visualizar_remessa_arquivo_aprovador />,
            breadcrumbs: true
        },
        {
            path: 'analisar_remessa_arquivo_verificador/:remessa_id',
            element: <remessadearquivoView.analisar_remessa_documento_verificar />,
            breadcrumbs: true
        },
        {
            path: 'analisar_remessa_arquivo_aprovador/:remessa_id',
            element: <remessadearquivoView.analisar_remessa_documento_aprovador />,
            breadcrumbs: true
        },
        {
            path: 'listagemdedocumento',
            element: <remessadearquivoView.listagem_documentos />,
            breadcrumbs: true
        }
    ]

export default remessadearquivoViewRoute