import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const disciplinaView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/disciplina'))),
    new : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/disciplina/new`))),
    view : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/disciplina/view`))),
    edit : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/disciplina/edit`)))
}

const disciplinaViewRoute =
    [
        {
            path: 'index',
            element: <disciplinaView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <disciplinaView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <disciplinaView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <disciplinaView.new />,
            breadcrumbs: true
        }
    ]

export default disciplinaViewRoute