import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const trechoView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/trechos'))),
    new : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/trechos/new`))),
    view : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/trechos/view`))),
    edit : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/trechos/edit`)))
}

const trechoViewRoute =
    [
        {
            path: 'index',
            element: <trechoView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <trechoView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <trechoView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <trechoView.new />,
            breadcrumbs: true
        }
    ]

export default trechoViewRoute