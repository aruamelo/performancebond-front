import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const subcontratosView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/subcontratos'))),
    new : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/subcontratos/new`))),
    view : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/subcontratos/view`))),
    edit : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/subcontratos/edit`)))
}

const subcontratosViewRoute =
    [
        {
            path: 'index',
            element: <subcontratosView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <subcontratosView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <subcontratosView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <subcontratosView.new />,
            breadcrumbs: true
        }
    ]

export default subcontratosViewRoute