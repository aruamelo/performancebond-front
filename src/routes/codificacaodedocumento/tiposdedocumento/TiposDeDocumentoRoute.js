import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const TiposDeDocumentoView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/tiposdedocumento'))),
    new : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/tiposdedocumento/new`))),
    view : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/tiposdedocumento/view`))),
    edit : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/tiposdedocumento/edit`)))
}

const TiposDeDocumentoViewRoute =
    [
        {
            path: 'index',
            element: <TiposDeDocumentoView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <TiposDeDocumentoView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <TiposDeDocumentoView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <TiposDeDocumentoView.new />,
            breadcrumbs: true
        }
    ]

export default TiposDeDocumentoViewRoute