import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const codificacaodedocumentoView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/codificacaodedocumento'))),
    new : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/codificacaodedocumento/new`))),
    view : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/codificacaodedocumento/view`))),
    edit : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/codificacaodedocumento/edit`)))
}

const codificacaodedocumentoViewRoute =
    [
        {
            path: 'index',
            element: <codificacaodedocumentoView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <codificacaodedocumentoView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <codificacaodedocumentoView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <codificacaodedocumentoView.new />,
            breadcrumbs: true
        }
    ]

export default codificacaodedocumentoViewRoute