import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const nucleoView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/nucleos'))),
    new : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/nucleos/new`))),
    view : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/nucleos/view`))),
    edit : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/nucleos/edit`)))
}

const nucleoViewRoute =
    [
        {
            path: 'index',
            element: <nucleoView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <nucleoView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <nucleoView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <nucleoView.new />,
            breadcrumbs: true
        }
    ]

export default nucleoViewRoute