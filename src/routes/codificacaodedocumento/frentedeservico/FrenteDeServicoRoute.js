import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const frenteDeServicoView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/frentedeservico'))),
    new : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/frentedeservico/new'))),
    view : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/frentedeservico/view'))),
    edit : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/frentedeservico/edit')))
}

const frenteDeServicoViewRoute =
    [
        {
            path: 'index',
            element: <frenteDeServicoView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <frenteDeServicoView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <frenteDeServicoView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <frenteDeServicoView.new />,
            breadcrumbs: true
        }
    ]

export default frenteDeServicoViewRoute