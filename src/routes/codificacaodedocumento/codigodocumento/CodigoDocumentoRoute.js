import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const codigoDocumentoView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/codigodocumento'))),
    new : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/codigodocumento/new'))),
    view : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/codigodocumento/view'))),
    edit : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/codigodocumento/edit')))
}

const codigodocumentoViewRoute =
    [
        {
            path: 'index',
            element: <codigoDocumentoView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <codigoDocumentoView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <codigoDocumentoView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <codigoDocumentoView.new />,
            breadcrumbs: true
        }
    ]

export default codigodocumentoViewRoute