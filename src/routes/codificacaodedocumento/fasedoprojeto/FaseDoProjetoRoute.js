import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const faseDoProjetoView = {
    index : Loadable(lazy(() => import('../../../views/pages/codificacaodedocumento/fasedoprojeto'))),
    new : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/fasedoprojeto/new`))),
    view : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/fasedoprojeto/view`))),
    edit : Loadable(lazy(() => import(`../../../views/pages/codificacaodedocumento/fasedoprojeto/edit`)))
}

const faseDoProjetoViewRoute =
    [
        {
            path: 'index',
            element: <faseDoProjetoView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <faseDoProjetoView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <faseDoProjetoView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <faseDoProjetoView.new />,
            breadcrumbs: true
        }
    ]

export default faseDoProjetoViewRoute