import Loadable from "../../../ui-component/Loadable";
import { lazy, React } from 'react'

const mascaraOficioEnviadoRouteView = {
    index : Loadable(lazy(() => import('../../../views/pages/administrativo/mascaraoficioenviado'))),
    new : Loadable(lazy(() => import(`../../../views/pages/administrativo/mascaraoficioenviado/new`))),
    view : Loadable(lazy(() => import(`../../../views/pages/administrativo/mascaraoficioenviado/view`))),
    edit : Loadable(lazy(() => import(`../../../views/pages/administrativo/mascaraoficioenviado/edit`)))
}

const mascaraOficioEnviadoRouteViewRoute =
    [
        {
            path: 'index',
            element: <mascaraOficioEnviadoRouteView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <mascaraOficioEnviadoRouteView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <mascaraOficioEnviadoRouteView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <mascaraOficioEnviadoRouteView.new />,
            breadcrumbs: true
        }
    ]

export default mascaraOficioEnviadoRouteViewRoute