import { lazy, React } from 'react'

// project imports
import MainLayout from '../layout/MainLayout'
import Loadable from '../ui-component/Loadable'

import UsuarioDoSistemaRoute from './usuariodosistema/UsuarioDoSistemaRoute'
import FuncionalidadeRoute from './permissoes/funcionalidade/FuncionalidadeRoute'
import grupoDeAcessoRoute from "./permissoes/grupodeacesso/GrupoDeAcessoRoute";

import CodigoDocumentoRoute from "./codificacaodedocumento/codigodocumento/CodigoDocumentoRoute";
import FrenteDeServicoRoute from "./codificacaodedocumento/frentedeservico/FrenteDeServicoRoute";
import TrechosRoute from "./codificacaodedocumento/trechos/TrechosRoute";
import NucleoRoute from "./codificacaodedocumento/nucleos/NucleoRoute";
import FaseDoProjetoRoute from "./codificacaodedocumento/fasedoprojeto/FaseDoProjetoRoute";
import DisciplinaRoute from "./codificacaodedocumento/disciplina/DisciplinaRoute";
import TiposDeDocumentoRoute from "./codificacaodedocumento/tiposdedocumento/TiposDeDocumentoRoute";
import SubcontratosRoute from "./codificacaodedocumento/subcontratos/SubcontratosRoute";
import CodificacaoDeDocumentoRoute from "./codificacaodedocumento/codificacaodedocumento/CodificacaoDeDocumentoRoute";
import RemessaDeArquivoRoute from "./remessadearquivo/RemessaDeArquivoRoute";
import ProjetosRoute from "./projetos/ProjetosRoute";
import MascaraOficioEnviadoRoute from "./administrativo/mascaraoficioenviado/MascaraOficioEnviadoRoute";

// import MascaraOficioEnviadoRoute from "./administrativo/mascaraoficioenviado/MascaraOficioEnviadoRoute";

// Importação dos componentes para o usuário visualizar em tela
const DashboardDefault = Loadable(lazy(() => import('../views/dashboard/Default')))
const ProfileUser = Loadable(lazy(() => import('../views/pages/usuario/profile')))

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  path: '/',
  element: <MainLayout />,
  children: [
    {
      path: '/',
      element: <DashboardDefault />
    },
    {
      path: 'dashboard',
      children: [
        {
          path: 'default',
          element: <DashboardDefault />
        }
      ]
    },
    {
      path: 'user/account-profile/profile1',
      breadcrumbs: true,
      element: <ProfileUser />
    },
    {
      path: 'pages/administrativo',
      children: [
        {
          path: 'mascaraoficioenviado',
          breadcrumbs: true,
          children: MascaraOficioEnviadoRoute
        }
      ]
    },
    {
      path: 'pages/remessadearquivo',
      children: [
        {
          path: 'entradadedocumento',
          breadcrumbs: true,
          children: RemessaDeArquivoRoute
        }
      ]
    },
    {
      path: 'pages',
      children: [
        {
          path: 'projetos',
          breadcrumbs: true,
          children: ProjetosRoute
        }
      ]
    },
    {
      path: 'pages/parametrizacaodosistema/codificacaodedocumento',
      children: [
        {
          path: 'codigodocumento',
          breadcrumbs: true,
          children: CodigoDocumentoRoute
        },
        {
          path: 'frentedeservico',
          breadcrumbs: true,
          children: FrenteDeServicoRoute
        },
        {
          path: 'trechos',
          breadcrumbs: true,
          children: TrechosRoute
        },
        {
          path: 'nucleo',
          breadcrumbs: true,
          children: NucleoRoute
        },
        {
          path: 'fasedoprojeto',
          breadcrumbs: true,
          children: FaseDoProjetoRoute
        },
        {
          path: 'disciplinas',
          breadcrumbs: true,
          children: DisciplinaRoute
        },
        {
          path: 'tiposdedocumento',
          breadcrumbs: true,
          children: TiposDeDocumentoRoute
        },
        {
          path: 'subcontratos',
          breadcrumbs: true,
          children: SubcontratosRoute
        },
        {
          path: 'parametrizacaocodificacaodedocumento',
          breadcrumbs: true,
          children: CodificacaoDeDocumentoRoute
        },
      ]
    },
    // Módulo Permissões
    {
      path: 'pages/permissoes',
      breadcrumbs: true,
      children: [
        {
          path: 'usuariodosistema',
          breadcrumbs: true,
          children: UsuarioDoSistemaRoute
        },
        {
          path: 'funcionalidades',
          breadcrumbs: true,
          children: FuncionalidadeRoute
        },
        {
          path: 'grupodeacesso',
          children: grupoDeAcessoRoute
        }
      ]
    }
  ]
}

export default MainRoutes
