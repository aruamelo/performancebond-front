import Loadable from "../../ui-component/Loadable";
import { lazy, React } from 'react'

const usuarioDosistemaView = {
    index : Loadable(lazy(() => import('../../views/pages/permissoes/usuariodosistema'))),
    new : Loadable(lazy(() => import('../../views/pages/permissoes/usuariodosistema/new'))),
    view : Loadable(lazy(() => import('../../views/pages/permissoes/usuariodosistema/view'))),
    edit : Loadable(lazy(() => import('../../views/pages/permissoes/usuariodosistema/edit')))
}

const UsuarioDoSistemaRoute =
    [
        {
            path: 'index',
            element: <usuarioDosistemaView.index />,
            breadcrumbs: true
        },
        {
            path: 'view/:id',
            element: <usuarioDosistemaView.view />,
            breadcrumbs: true
        },
        {
            path: 'edit/:id',
            element: <usuarioDosistemaView.edit />,
            breadcrumbs: true
        },
        {
            path: 'new',
            element: <usuarioDosistemaView.new />,
            breadcrumbs: true
        }
    ]

export default UsuarioDoSistemaRoute