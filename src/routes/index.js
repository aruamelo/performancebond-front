import { useRoutes } from 'react-router-dom'

// routes
import MainRoutes from './MainRoutes'
import ContratoUser from './ContratoUserRoutes'
import LoginRoutes from './LoginRoutes'

// ==============================|| ROUTING RENDER ||============================== //

export default function ThemeRoutes () {
  return useRoutes([LoginRoutes, MainRoutes, ContratoUser])
}
