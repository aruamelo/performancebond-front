import { lazy, React } from 'react'

// project imports
import MainLayouContratoUser from '../layout/MainLayout/indexContratoUser'
import Loadable from '../ui-component/Loadable'

// dashboard routing
const ContratoUserRoutes = Loadable(lazy(() => import('../views/dashboard/ContratoUser')))

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  path: 'contrato/user',
  element: <MainLayouContratoUser />,
  children: [
    {
      path: 'index',
      element: <ContratoUserRoutes />
    }
  ]
}

export default MainRoutes
