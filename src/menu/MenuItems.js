
import { useState, useEffect, useRef, React } from 'react'
import Dropdown from './Dropdown'

import { Link } from 'react-router-dom'
// eslint-disable-next-line
const MenuItems = ({ items, depthLevel }) => {
  const [dropdown, setDropdown] = useState(false)

  const ref = useRef()

  useEffect(() => {
    const handler = (event) => {
      if (
        dropdown &&
        ref.current &&
        !ref.current.contains(event.target)
      ) {
        setDropdown(false)
      }
    }
    document.addEventListener('mousedown', handler)
    document.addEventListener('touchstart', handler)
    return () => {
      // Cleanup the event listener
      document.removeEventListener('mousedown', handler)
      document.removeEventListener('touchstart', handler)
    }
  }, [dropdown])

  const onMouseEnter = () => {
    window.innerWidth > 960 && setDropdown(true)
  }

  const onMouseLeave = () => {
    window.innerWidth > 960 && setDropdown(false)
  }

  const closeDropdown = () => {
    dropdown && setDropdown(false)
  }

  return (
    <li
      className="menu-items"
      ref={ref}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onClick={closeDropdown}
    >
      {
      // eslint-disable-next-line
      items.url && items.submenu
        ? (
        <>
          <button
            type="button"
            aria-haspopup="menu"
            aria-expanded={dropdown ? 'true' : 'false'}
            onClick={() => setDropdown((prev) => !prev)}
          >
            {window.innerWidth < 960 && depthLevel === 0
              ? (
                  // eslint-disable-next-line
                  items.title
                )
              : (
                // eslint-disable-next-line
              <Link to={items.url}>{items.title}</Link>
                )}

            {depthLevel > 0 &&
            window.innerWidth < 960
              ? null
              : depthLevel > 0 &&
              window.innerWidth > 960
                ? (
              <span>&raquo;</span>
                  )
                : (
              <span className="arrow" />
                  )}
          </button>
          <Dropdown
            // eslint-disable-next-line
            depthLevel={depthLevel}
            // eslint-disable-next-line
            submenus={items.submenu}
            dropdown={dropdown}
          />
        </>
          // eslint-disable-next-line
          ) :
          // eslint-disable-next-line
          !items.url && items.submenu
            ? (
        <>
          <button
            type="button"
            aria-haspopup="menu"
            aria-expanded={dropdown ? 'true' : 'false'}
            onClick={() => setDropdown((prev) => !prev)}
          >
            {
              // eslint-disable-next-line
              items.title
            }{' '}
            {depthLevel > 0
              ? (
              <span>&raquo;</span>
                )
              : (
              <span className="arrow" />
                )}
          </button>
          <Dropdown
            depthLevel={depthLevel}
            // eslint-disable-next-line
            submenus={items.submenu}
            dropdown={dropdown}
          />
        </>
              )
            : (
              // eslint-disable-next-line
        <Link to={items.url}>{items.title}</Link>
              )}
    </li>
  )
}

export default MenuItems
