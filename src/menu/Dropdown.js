import { React } from 'react'
import MenuItems from './MenuItems'
// eslint-disable-next-line
const Dropdown = ({ submenus, dropdown, depthLevel }) => {
  // eslint-disable-next-line
  depthLevel = depthLevel + 1
  const dropdownClass = depthLevel > 1 ? 'dropdown-submenu' : ''
  return (
    <ul
      className={`dropdown ${dropdownClass} ${
        dropdown ? 'show' : ''
      }`}
    >
      {
      // eslint-disable-next-line
      submenus.map((submenu, index) => (
        <MenuItems
          items={submenu}
          key={index}
          depthLevel={depthLevel}
        />
      ))}
    </ul>
  )
}

export default Dropdown
