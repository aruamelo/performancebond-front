import Noty from 'noty'

import '../../node_modules/noty/lib/noty.css'
import '../../node_modules/noty/lib/themes/mint.css'

const noty = {
  success: (text) => {
    new Noty({
      type: 'success',
      layout: 'top',
      text,
      timeout: 5000
    }).show()
  },
  error: (text) => {
    new Noty({
      type: 'error',
      layout: 'top',
      text,
      timeout: 5000
    }).show()
  },
  warning: (text) => {
    new Noty({
      type: 'warning',
      layout: 'top',
      text,
      timeout: 5000
    }).show()
  },
  info: (text) => {
    new Noty({
      type: 'info/information',
      layout: 'top',
      text,
      timeout: 5000
    }).show()
  }
}

export default noty
