import request from '../../request/request'
const urlPadrao = 'remessadearquivo/remessadearquivo'

const RemessaDeArquivoService = {
  getRemessasArquivo: async (contrato_id) => {
    return request.get('remessadearquivo/remessassubmetidas', {contrato_id})
  },
  showRemessaArquivo: async (id) => {
    const data = {id}
    return request.get(urlPadrao, data)
  },
  viewdwf: async (arquivo_id) => {
    return request.get(`remessadearquivo/visualizardwf`, {arquivo_id})
  },
  finalizarRemessa: async (data) => {
    return request.post(`remessadearquivo/finalizarremessa`, data)
  },
  submeterDocumento: async (data) => {
    return request.post(urlPadrao, data)
  },
  getCampos: async (data) => {
    data = {contrato_id: data}
    return request.get('remessadearquivo/campos', data)
  },
  validarNomeArquivo: async (valores) => {
    return request.postFile(`remessadearquivo/validationnamefile`, valores)
  },
  atualizarTitulo: async (titulos) => {
    return request.put('remessadearquivo/tituloarquivo', titulos)
  },
  cancelarRemessa: async (remessa_id) => {
    return request.put('remessadearquivo/cancelarremessa', {remessa_id})
  },
  finalizarRemessaDocumentoSubmetido: async (remessa_id) => {
    return request.put('remessadearquivo/finalizarremessadocumentosubmetido', {remessa_id})
  },
  showRemessaDocumentoSubmetido: async (remessa_id) => {
    return request.get('remessadearquivo/showremessaarquivo', {remessa_id})
  }
}

export default RemessaDeArquivoService
