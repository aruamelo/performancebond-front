import remessaDeArquivoService from "../remessadearquivo/RemessaDeArquivoService";
import config from "../../config";

const DownloadArquivoService = {
    downloadArquivo : async (arquivo_id) => {
        remessaDeArquivoService.viewdwf(arquivo_id).then((data) => {
           let response =  data.data

            const link = document.createElement('a');
            link.href = DownloadArquivoService.mountLinkDownload(response.arquivo);
            link.setAttribute('download', response.nome); // Nome do arquivo que será baixado
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        });
    },
    mountLinkDownload: (nomeArquivo) => {
        let hostDownload = config.urlApi.replace('/api', '')

        return `${hostDownload}/autodesk/remessadearquivo/visualizardwf/${nomeArquivo}`
    }
}


export default DownloadArquivoService
