import downloadArquivoService from "../download/DownloadArquivoService";

const AutodeskService = {
    initializeViewerOffline : async (responseArquivo) => {
        // Adicionar o link para o arquivo CSS
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = 'https://developer.api.autodesk.com/modelderivative/v2/viewers/style.min.css?v=v7.*';
        link.type = 'text/css';
        document.head.appendChild(link);

        // Inicialize o visualizador offline aqui
        const script = document.createElement('script');
        script.src = 'https://developer.api.autodesk.com/modelderivative/v2/viewers/viewer3D.min.js?v=v7.*'; // Caminho para o arquivo JavaScript do Autodesk Viewer
        script.async = true;
        script.onload = () => {
            const myViewerDiv = document.getElementById('viewer-container');
            const viewer = new window.Autodesk.Viewing.GuiViewer3D(myViewerDiv);
            let linkDownload = downloadArquivoService.mountLinkDownload(responseArquivo.arquivo)

            const options = {
                env: 'Local',
                document: linkDownload
            };

            window.Autodesk.Viewing.Initializer(options, function() {
                viewer.start(options.document, options);
            });
        };
        document.body.appendChild(script);
    }
}

export default AutodeskService

