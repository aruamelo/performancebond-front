import request from '../../request/request'

const grupoDeAcessoService = {
  gruposdeacesso: async () => {
    return request.get('grupodeacesso/gruposdeacesso')
  },
  showGrupoDeAcessoService: async (id) => {
    const data = {id}
    return request.get('grupodeacesso/grupodeacesso', data)
  },
  editGrupoDeAcessoService: async (data) => {
    return request.put('grupodeacesso/update', data)
  },
  deleteGrupoDeAcessoService: async (data) => {
    return request.delete('grupodeacesso/delete', data)
  },
  insertGrupoDeAcessoService: async (data) => {
    return request.post('grupodeacesso/inserir', data)
  },
}

export default grupoDeAcessoService
