import request from '../../request/request'

const usuarioDoSistemaService = {
  funcionalidades: async () => {
    return request.get('funcionalidade/funcionalidades')
  },
  showFuncionalidade: async (id) => {
    const data = {id}
    return request.get('funcionalidade/funcionalidade', data)
  },
  editFuncionalidade: async (data) => {
    return request.put('funcionalidade/update', data)
  },
  deleteFuncionalidade: async (data) => {
    return request.delete('funcionalidade/delete', data)
  },
  insertFuncionalidade: async (data) => {
    return request.post('funcionalidade/inserir', data)
  },
}

export default usuarioDoSistemaService
