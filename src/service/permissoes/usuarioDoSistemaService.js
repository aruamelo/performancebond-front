import request from '../../request/request'

const usuarioDoSistemaService = {
  usuariosPorContrato: async (idContrato) => {
    const data = { idContrato }
    return request.get('usuariodosistema/usuarioporcontrato', data)
  },
  showUsuario: async (idUser) => {
    const data = {idUser}
    return request.get('usuariodosistema/showusuarioporcontrato', data)
  },
  editUsuario: async (idContratoUser) => {
    const data = { idUser: idContratoUser }
    return request.get('usuariodosistema/usuario', data)
  }
}

export default usuarioDoSistemaService
