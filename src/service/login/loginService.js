import request from '../../request/request'
import {jwtDecode} from 'jwt-decode';
import distributedFunctions from "../../request/distributedFunctions";

const loginService = {
  login: async (email, password) => {
    const data = { email, password }
    return request.login('auth/login', data)
  },
  validarPermissao: (permissao) => {
      let auth = distributedFunctions.getDataUser()
      const informacoes = jwtDecode(auth.access_token);

      if (!informacoes.permission.includes(permissao)) {
        return false
      }

      return true
  }
}

export default loginService
