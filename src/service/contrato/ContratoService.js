import request from '../../request/request'

const ContratoService = {
  contratoPorUsuario: async (idUsuario) => {
    const data = { user_id: idUsuario }
    return request.get('contrato/contratoporusuario', data)
  }
}

export default ContratoService
