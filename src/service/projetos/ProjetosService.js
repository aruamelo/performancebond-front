import request from '../../request/request'
import distributedFunctions from "../../request/distributedFunctions";

const ProjetosService = {
    getListaDocumentosSubmetidosVerificador: async (contrato_id) => {
        return request.get('analisarremessa/remessaarquivoverficador', {contrato_id})
    },
    getDocumentosRemessaParaVerificar: async (remessa_id) => {
        return request.get('analisarremessa/analisarremessaverificador', {remessa_id})
    },
    getSituacaoAnalise: async () => {
        return request.get('analisarremessa/situacaoanalise')
    },
    salvarRascunhoAnaliseVerificador: async(dadosRascunho) => {
        return request.post('analisarremessa/rascunhoanaliseremessaverificador', dadosRascunho)
    },
    finalizarAnaliseVerificador: async(dadosRascunho) => {
        return request.post('analisarremessa/finalizaranaliseremessaverificador', dadosRascunho)
    },
    getListaDocumentosSubmetidosAprovador: async (contrato_id) => {
        return request.get('analisarremessa/remessaarquivoaprovador', {contrato_id})
    },
    getDocumentosRemessaParaAprovar: async (remessa_id) => {
        return request.get('analisarremessa/analisarremessaaprovador', {remessa_id})
    },
    salvarRascunhoAnaliseAprovador: async(dadosRascunho) => {
        return request.post('analisarremessa/rascunhoanaliseremessaaprovador', dadosRascunho)
    },
    finalizarAnaliseAprovador: async(dadosRascunho) => {
        return request.post('analisarremessa/finalizaranaliseremessaaprovador', dadosRascunho)
    },
    formatarDadosEnviar: (arquivos, remessa_id, selectedAnalise, rascunhoAnaliseAtualizados) => {
        let dadosRascunho = {}; // Inicializa como um objeto vazio

        dadosRascunho['remessa_id'] = remessa_id;

        dadosRascunho['arquivos'] = arquivos.map((arquivo) => {
            let itemArquivo = {}; // Inicializa como um objeto vazio
            itemArquivo["arquivo_analise_id"] = arquivo.arquivo_id;
            itemArquivo["contrato_id"] = distributedFunctions.getIdContratoUser();
            itemArquivo["situacao_id"] = selectedAnalise[arquivo.arquivo_id];
            itemArquivo["justificativa"] = rascunhoAnaliseAtualizados[arquivo.arquivo_id];
            itemArquivo["remessa_id"] = remessa_id;

            return itemArquivo;
        });

        return dadosRascunho;
    },
    listagemDocumentoNivelUm: (contrato_id) => {
        return request.get('documentos/aprovadosnivelum', {contrato_id})
    },
    listagemDocumentoProximoNo: (data) => {
        return request.post('documentos/nosdocumentosaprovados', data)
    }
}

export default ProjetosService