import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";

const MascaraOficioEnviadoService = {
  mascaraOficioPorContrato: async () => {
    const idContrato = distributedFunctions.getIdContratoUser()
    const data = { contrato_id:idContrato }
    return request.get('administrativo/mascaraoficioenviado/mascaras', data)
  },
  showMascaraOficio: async (id) => {
    const data = {id}
    return request.get('administrativo/mascaraoficioenviado/mascara', data)
  },
  editMascaraOficio: async (id) => {
    const data = { id }
    return request.get('administrativo/mascaraoficioenviado/mascara', data)
  },
  updateMascaraOficio: async (data) => {
    return request.put('administrativo/mascaraoficioenviado/mascara', data)
  },
  insertMascaraOficio: async (data) => {
    return request.post('administrativo/mascaraoficioenviado/mascara', data)
  }
}

export default MascaraOficioEnviadoService
