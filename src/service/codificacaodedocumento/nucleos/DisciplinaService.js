import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/disciplinas/disciplinas'

const disciplinaService = {
  disciplinas: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get(urlPadrao, data)
  },
  showDisciplina: async (id) => {
    const data = {id}
    return request.get('codificacaodedocumento/disciplinas/disciplina', data)
  },
  editDisciplina: async (data) => {
    return request.put(urlPadrao, data)
  },
  deleteDisciplina: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertDisciplina: async (data) => {
    return request.post(urlPadrao, data)
  },
}

export default disciplinaService
