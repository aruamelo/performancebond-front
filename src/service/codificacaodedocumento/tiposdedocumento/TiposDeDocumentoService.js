import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/tiposdedocumento/tiposdedocumento'

const tiposDeDocumentoService = {
  tiposDeDocumento: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get(urlPadrao, data)
  },
  showTipoDeDocumento: async (id) => {
    const data = {id}
    return request.get('codificacaodedocumento/tiposdedocumento/tipodedocumento', data)
  },
  editTipoDeDocumento: async (data) => {
    return request.put(urlPadrao, data)
  },
  deleteTipoDeDocumento: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertTipoDeDocumento: async (data) => {
    return request.post(urlPadrao, data)
  },
}

export default tiposDeDocumentoService
