import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/fasedoprojeto/fasesdoprojeto'

const faseDoProjetoService = {
  fasesDoProjetoService: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get(urlPadrao, data)
  },
  showFaseDoProjeto: async (id) => {
    const data = {id}
    return request.get('codificacaodedocumento/fasedoprojeto/fasedoprojeto', data)
  },
  editFaseDoProjeto: async (data) => {
    return request.put(urlPadrao, data)
  },
  deleteFaseDoProjeto: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertFaseDoProjeto: async (data) => {
    return request.post(urlPadrao, data)
  },
}

export default faseDoProjetoService
