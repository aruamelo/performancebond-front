import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/codigodocumento/codigosdocumento'

const codigoDocumentoService = {
  codigosDocumento: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get(urlPadrao, data)
  },
  showcodigoDocumento: async (id) => {
    const data = {id}
    return request.get('codificacaodedocumento/codigodocumento/codigodocumento', data)
  },
  editcodigoDocumento: async (data) => {
    return request.put(urlPadrao, data)
  },
  deletecodigoDocumento: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertcodigoDocumento: async (data) => {
    return request.post(urlPadrao, data)
  },
}

export default codigoDocumentoService
