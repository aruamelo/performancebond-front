import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/codificacaodedocumento/codificacaodedocumento'

const codificacaoDeDocumentoService = {
  codificacoesDeDocumento: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get('codificacaodedocumento/codificacaodedocumento/codificacoesdosdocumentos', data)
  },
  showCodificacaoDeDocumento: async (id) => {
    const data = {id}
    return request.get(urlPadrao, data)
  },
  editCodificacaoDeDocumento: async (data) => {
    return request.put(urlPadrao, data)
  },
  deleteCodificacaoDeDocumento: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertCodificacaoDeDocumento: async (data) => {
    return request.post(urlPadrao, data)
  },
  getCamposParametrizarCodificacaoDeDocumento: async () => {
    return request.get('codificacaodedocumento/codificacaodedocumento/camposcodificacaodedocumento')
  }
}

export default codificacaoDeDocumentoService
