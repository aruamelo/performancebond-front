import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/subcontratos/subcontratos'

const subcontratosService = {
  subcontratos: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get(urlPadrao, data)
  },
  showSubcontrato: async (id) => {
    const data = {id}
    return request.get('codificacaodedocumento/subcontratos/subcontrato', data)
  },
  editSubcontrato: async (data) => {
    return request.put(urlPadrao, data)
  },
  deleteSubcontrato: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertSubcontrato: async (data) => {
    return request.post(urlPadrao, data)
  },
}

export default subcontratosService
