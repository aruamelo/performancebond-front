import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/frentedeservico/frentedeservicos'

const frenteDeServicoService = {
  frenteDeServicos: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get(urlPadrao, data)
  },
  showFrenteDeServico: async (id) => {
    const data = {id}
    return request.get('codificacaodedocumento/frentedeservico/frentedeservico', data)
  },
  editFrenteDeServico: async (data) => {
    return request.put(urlPadrao, data)
  },
  deleteFrenteDeServico: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertFrenteDeServico: async (data) => {
    return request.post(urlPadrao, data)
  },
}

export default frenteDeServicoService
