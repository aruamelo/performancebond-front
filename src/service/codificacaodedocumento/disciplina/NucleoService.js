import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/nucleo/nucleos'

const nucleoService = {
  nucleos: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get(urlPadrao, data)
  },
  showNucleo: async (id) => {
    const data = {id}
    return request.get('codificacaodedocumento/nucleo/nucleo', data)
  },
  editNucleo: async (data) => {
    return request.put(urlPadrao, data)
  },
  deleteNucleo: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertNucleo: async (data) => {
    return request.post(urlPadrao, data)
  },
}

export default nucleoService
