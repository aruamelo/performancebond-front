import request from '../../../request/request'
import distributedFunctions from "../../../request/distributedFunctions";
const urlPadrao = 'codificacaodedocumento/trechos/trechos'

const trechosService = {
  trechos: async () => {
    const data = {contrato_id: distributedFunctions.getIdContratoUser()}
    return request.get(urlPadrao, data)
  },
  showTrecho: async (id) => {
    const data = {id}
    return request.get('codificacaodedocumento/trechos/trecho', data)
  },
  editTrecho: async (data) => {
    return request.put(urlPadrao, data)
  },
  deleteTrecho: async (data) => {
    return request.delete(urlPadrao, data)
  },
  insertTrecho: async (data) => {
    return request.post(urlPadrao, data)
  },
}

export default trechosService
