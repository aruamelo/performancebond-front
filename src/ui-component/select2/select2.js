import React, { useEffect, useRef } from 'react';
import 'select2/dist/css/select2.min.css';
import 'select2/dist/js/select2.full.min.js';
import $ from 'jquery'; // Importe o jQuery
import config from "../../config";
import distributedFunctions from "../../request/distributedFunctions";
const Select2Async = (parameter) => {

    const selectRef = useRef(null);
    const user = distributedFunctions.getDataUser()
    const placeholder = parameter.placeholder ?? 'Selecionar um item'

    if (parameter.value != undefined) {
        Object.entries(parameter.value).forEach(([key, value]) => {
            let newOption = new Option(value, key, true, true);
            $(selectRef.current).append(newOption).trigger('change');
        });
    }

    useEffect(() => {
        // Inicialize o Select2 no componente montado
        $(selectRef.current).select2({
            placeholder: placeholder,
            width: '100%',
            allowClear: true,
            height: '150px',
            minimumInputLength: 2,
            ajax: {
                headers: {"Authorization":  "Bearer " + user.access_token},
                url: `${config.urlApi}/${parameter.url}`, // Endpoint para buscar dados assíncronos
                data: function (params) {
                    let query = {
                        search: params.term,
                        page: params.page || 1
                    }

                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                dataType: 'json',
                delay: 250, // Tempo de espera antes de fazer a solicitação AJAX após o usuário digitar (em milissegundos)
                processResults: function (data) {
                    // Mapeie os dados para o formato esperado pelo Select2
                    return {
                        results: data.map(item => ({
                            id: item.id,
                            text: item.text,
                        })),
                    };
                },
                cache: true
            },
        });

        if (parameter.formik != null) {
            $(selectRef.current).on('change', (e) => {
                parameter.formik.setFieldValue(parameter.name, e.target.value);
            });
        }
    }, []);

    return <select ref={selectRef} />;
};

export default Select2Async;
