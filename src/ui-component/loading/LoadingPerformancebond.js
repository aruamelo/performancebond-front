import React from 'react';
import { BlockUI } from 'primereact/blockui';
import { ProgressSpinner } from 'primereact/progressspinner';

const BlockingSpinner = (parameter) => {
    return (
        <div style={{position: 'relative', minHeight: '100vh'}}>
            <BlockUI blocked={parameter.loading} fullScreen>
                {parameter.loading &&
                    <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                        <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                            Carregando... <br/>
                        </div>
                        <ProgressSpinner style={{width: '400px', height: '400px'}} strokeWidth="2"
                                         animationDuration=".5s"/>
                    </div>
                }
            </BlockUI>
        </div>
    );
};

export default BlockingSpinner;
