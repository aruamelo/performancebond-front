import React, { useState, useEffect } from 'react'
import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { Button } from 'primereact/button'
import Stack from 'react-bootstrap/Stack'
import request from '../../request/request'
import noty from '../../alert/noty'
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import distributedFunctions from '../../request/distributedFunctions'

export function DatatableCustom (parameter) {
  const [products, setProducts] = useState([])
  const [lengthrow, setLengthRow] = useState([])
  const [first, setFirst] = useState(0)
  let rows = null
  // const dt = React.useRef(null);

  // Definir as colunas que serão exibidas para o usuário
  const columns = parameter.columns
  // Criar as colunas dinâmicas conforme as colunas enviadas no arquivo principal
  const dynamicColumns = columns.map((col) => {
    return <Column key={col.field} field={col.field} header={col.header} filter sortable/>
  })

    const exportColumns = columns.map(col => ({ title: col.header, dataKey: col.field }));


    dynamicColumns.push(<Column key="acaoBotao" field="acaoBotao" header="Ação" />)

  /**
   * Método responsável em enviar a requisição via ajax para deletar o registro
   * @param id
   */
    const acceptFunc = (id) => {
        request.delete(id, parameter.urlDelete).then(data => {
        noty.success(data.data.message)

        // Filtrar as informações de todas as linhas para remover o item selecionado
        let row = rows.filter(item => {
          return item.id !== id
        })

        setProducts(row)
        rows = row
    }).catch((err) => {
        let responseErro = JSON.parse(err.request.response)
        noty.error(responseErro.message)
    })
  }

  /**
   * Método responsável em excluir o registro
   * Se quiser passar uma URL customizada, basta criar uma propriedade
   * urlCustomDelete no datatable
   * @param {*} id
   */
  const deleteClick = (id) => {
    confirmDialog({
      message: 'Deseja deletar o registro?',
      header: 'Deletar o registro',
      icon: 'pi pi-info-circle',
      acceptLabel: 'Sim',
      rejectLabel: 'Não',
      accept: () => acceptFunc(id)
    });
  }

  useEffect(() => {
    // Recuperar as linhas para exibição
    rows = parameter.row
    const viewButtonDefault = parameter.viewButtonDefault ?? true

    if (viewButtonDefault) {
      rows.map((register) => {
        register.acaoBotao =
            <Stack direction="horizontal" gap={3}>
              <Button icon="pi pi-eye"
                      tooltip="Visualizar"
                      tooltipOptions={{ position: 'bottom' }}
                      rounded className="p-2"
                      severity="secondary"
                      onClick={() => distributedFunctions.redirectPage('view', register.id)}/>

              <Button icon="pi pi-file-edit"
                      tooltip="Editar"
                      tooltipOptions={{ position: 'bottom' }}
                      rounded className="p-2"
                      onClick={() => distributedFunctions.redirectPage( 'edit', register.id)}/>

              <Button icon="pi pi-trash"
                      tooltip="Deletar"
                      tooltipOptions={{ position: 'bottom' }}
                      rounded
                      severity="danger"
                      onClick={() => deleteClick(register.id)}/>
            </Stack>
      })
      setProducts(rows)
    }

   if (!viewButtonDefault) {
       setProducts(rows)
   }
    // Por padrão serão exibidos 25 linhas
    const lengthRow = parameter.lengthRow ?? 25
    setLengthRow(lengthRow)
  }, [])

    // const exportCSV = (selectionOnly) => {
    //     dt.current.exportCSV({ selectionOnly });
    // }

    const exportPdf = () => {
        import('jspdf').then(jsPDF => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);
                doc.autoTable(exportColumns, products);
                let namePdf = 'performancebond_export_' + new Date().getTime() + '.pdf'
                doc.save(namePdf);
            })
        })
    }

    const exportExcel = () => {
        import('xlsx').then(xlsx => {
            const worksheet = xlsx.utils.json_to_sheet(products);
            const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
            saveAsExcelFile(excelBuffer, 'performancebond');
        });
    }

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then(module => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });

                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    }

    const header = (
        <div className="flex align-items-center export-buttons">
            {/*<Button type="button" icon="pi pi-file" onClick={() => exportCSV(false)} className="mr-2" data-pr-tooltip="CSV" />*/}
            <Button type="button" icon="pi pi-file-excel" onClick={exportExcel} className="p-button-success mr-4" data-pr-tooltip="XLS" />
            <Button type="button" icon="pi pi-file-pdf" onClick={exportPdf} className="p-button-warning ml-4" data-pr-tooltip="PDF" />
            {/*<Button type="button" icon="pi pi-filter" onClick={() => exportCSV(true)} className="p-button-info ml-auto" data-pr-tooltip="Selection Only" />*/}
        </div>
    );

  return (
    <div className="card">
      <DataTable
        value={products}
        paginator
        rows={lengthrow}
        first={first}
        onPage={(e) => setFirst(e.first)}
        size="large"
        sortable="true"
        responsiveLayout="scroll"
        responsive = "true"
        dataKey="id"
        filterDisplay="row"
        emptyMessage="Registro não encontrado."
        header={header}
      >
        {dynamicColumns}
      </DataTable>
      <ConfirmDialog />
    </div>
  )
}
