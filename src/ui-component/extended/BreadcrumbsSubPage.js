import PropTypes from 'prop-types'
import { React } from 'react'
import { Link } from 'react-router-dom'

// material-ui
import { useTheme } from '@mui/material/styles'
import { Box, Card, Divider, Grid, Typography } from '@mui/material'
import MuiBreadcrumbs from '@mui/material/Breadcrumbs'

// project imports
import { gridSpacing } from '../../store/constant'

// assets
import { IconTallymark1 } from '@tabler/icons-react'
import AccountTreeTwoToneIcon from '@mui/icons-material/AccountTreeTwoTone'
import distributedFunctions from "../../request/distributedFunctions";
const linkSX = {
  display: 'flex',
  color: 'grey.900',
  textDecoration: 'none',
  alignContent: 'center',
  alignItems: 'center'
}

// ==============================|| BREADCRUMBS ||============================== //

const BreadcrumbsSubPage = (
  { card, divider, icons, maxItems, navigation, rightAlign, separator, title, titleBottom, ...others }) => {

    distributedFunctions.permissionPage(navigation.permission)

  const theme = useTheme()

  const iconStyle = {
    marginRight: theme.spacing(0.75),
    marginTop: `-${theme.spacing(0.25)}`,
    width: '1rem',
    height: '1rem',
    color: theme.palette.secondary.main
  }

  // item separator
  const SeparatorIcon = separator
  const separatorIcon =
  separator ? <SeparatorIcon stroke={1.5} size="1rem" /> : <IconTallymark1 stroke={1.5} size="1rem" />

  let mainContent
  let itemContent
  let breadcrumbContent = <Typography />
  let CollapseIcon
  let ItemIcon

  // collapse item
    CollapseIcon =  AccountTreeTwoToneIcon
    mainContent = (
        <Typography component={Link} to="#" variant="subtitle1" sx={linkSX}>
            {icons && <CollapseIcon style={iconStyle} />}
            {navigation.title}
        </Typography>)

  // items
    ItemIcon = navigation.icon ? navigation.icon : AccountTreeTwoToneIcon
    itemContent = (
            <Typography
                variant="subtitle1"
                sx={{
                  display: 'flex',
                  textDecoration: 'none',
                  alignContent: 'center',
                  alignItems: 'center',
                  color: 'grey.500'
                }}
            >
                {icons && <ItemIcon style={iconStyle} />}
                {navigation.titlepage}
            </Typography>
    )

      breadcrumbContent = (
                <Card
                    sx={{
                      marginBottom: card === false ? 0 : theme.spacing(gridSpacing),
                      border: card === false ? 'none' : '1px solid',
                      borderColor: theme.palette.primary[200] + 75,
                      background: card === false ? 'transparent' : theme.palette.background.default
                    }}
                    {...others}
                >
                    <Box sx={{ p: 2, pl: card === false ? 0 : 2 }}>
                        <Grid
                            container
                            direction={rightAlign ? 'row' : 'column'}
                            justifyContent={rightAlign ? 'space-between' : 'flex-start'}
                            alignItems={rightAlign ? 'center' : 'flex-start'}
                            spacing={1}
                        >
                            {title && !titleBottom && (
                                <Grid item>
                                    <Typography variant="h3" sx={{ fontWeight: 500 }}>
                                        {navigation.title}
                                    </Typography>
                                </Grid>
                            )}
                            <Grid item>
                                <MuiBreadcrumbs
                                    sx={{ '& .MuiBreadcrumbs-separator': { width: 16, ml: 1.25, mr: 1.25 } }}
                                    aria-label="breadcrumb"
                                    maxItems={maxItems || 8}
                                    separator={separatorIcon}
                                >
                                  {/* Ícone do */}
                                    {/* <Typography
                                        component={Link}
                                        to="#"
                                        color="inherit"
                                        variant="subtitle1"
                                        sx={linkSX}>
                                        {icons && <HomeTwoToneIcon sx={iconStyle} />}
                                        {icon && <HomeIcon sx={{ ...iconStyle, mr: 0 }} />}
                                        {!icon && 'Dashboard'}
                                    </Typography> */}
                                    {mainContent}
                                    {itemContent}
                                </MuiBreadcrumbs>
                            </Grid>
                        </Grid>
                    </Box>
                    {card === false && divider !== false &&
                    <Divider sx={{ borderColor: theme.palette.primary.main, mb: gridSpacing }} />}
                </Card>
      )

  return breadcrumbContent
}

BreadcrumbsSubPage.propTypes = {
  card: PropTypes.bool,
  divider: PropTypes.bool,
  icon: PropTypes.bool,
  icons: PropTypes.bool,
  maxItems: PropTypes.number,
  navigation: PropTypes.object,
  rightAlign: PropTypes.bool,
  separator: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  title: PropTypes.bool,
  titleBottom: PropTypes.bool
}

export default BreadcrumbsSubPage
