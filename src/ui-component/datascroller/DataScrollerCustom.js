import React from 'react';
import { DataScroller } from 'primereact/datascroller';
import '../../assets/datascroller/DataScrollerDemo.css';

export function DataScrollerCustom (parameter) {
    let value = parameter.rows
    let itemTemplate = parameter.itemTemplate
    let tituloGrid = parameter.tituloGrid

    return (

        <div className="datascroller-demo">
            <div className="card">
                <DataScroller value={value} itemTemplate={itemTemplate} rows={5} inline
                              scrollHeight="900px" header={tituloGrid} emptyMessage={'Nenhum registro encontrado'}/>
            </div>
        </div>
    )
}