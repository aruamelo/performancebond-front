import React from 'react'
import { DatatableCustom } from '../../ui-component/datatable/DatatableCustom'
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import { Button } from 'primereact/button'
import distributedFunctions from '../../request/distributedFunctions';

export function IndexDatatable (parameter) {
    let viewButtonCreate = parameter.viewButtonCreate ?? true
  return (
    <Container fluid>
        {
            viewButtonCreate ? (
                    <Row style={{marginBottom: '30px'}}>
                        <Col>
                            <Button
                                className="p-button-raised p-button-rounded"
                                tooltip="Novo"
                                tooltipOptions={{position: 'bottom'}}
                                icon="pi pi-plus"
                                onClick={() => distributedFunctions.redirectPage('new')}/>
                        </Col>
                    </Row>
            ) : null
        }
      <Row>
        <div className="card">
            <DatatableCustom
              columns={parameter.columns}
              row={parameter.data}
              urlDelete={parameter.urlDelete}
              viewButtonDefault = {parameter.viewButtonDefault}/>
        </div>
      </Row>
    </Container>
  )
}
