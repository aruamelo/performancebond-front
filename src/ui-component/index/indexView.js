import React from 'react'
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';

// import Breadcrumbs from "../extended/Breadcrumbs";
import { IconChevronRight } from '@tabler/icons-react'
import distributedFunctions from "../../request/distributedFunctions";
import { Button } from 'primereact/button'
import BreadcrumbsSubPage from "../extended/BreadcrumbsSubPage";
export function IndexView (parameter) {
  const data = parameter.data
  const quantidadeTotalCampoShow = data.length
  const quantidadeColunaPorLinha = parameter.colunaPorLinha ?? 4
  const quantidadeRow = Math.ceil(quantidadeTotalCampoShow / quantidadeColunaPorLinha, 1)

  // Separar o array para poder incluir conforme a row e col
  let arrayColumnRow = []
  for (let i = 0; i < data.length; i += quantidadeColunaPorLinha) {
    arrayColumnRow.push(data.slice(i, i + quantidadeColunaPorLinha));
  }

  let dynamicColumnsShow = []
  // Realizar o laço para incluir a quantidade de linhas com base na quantidade de campos e por colunas em sua linha
  for (let i = 0; i < quantidadeRow; i++) {
    // Percorrer o array dividido com base na quantidade de linhas
    let colRowCard = arrayColumnRow[i].map((dataColumn) => {
      return <Col key={dataColumn.field}>
                <div className="pt-3">
                  <label htmlFor={dataColumn.field} >{dataColumn.label}</label>
                </div>
                <div className="pt-3">
                  {dataColumn.value}
                </div>
              </Col>
    })
    // Incluir as colunas em cada linha
    dynamicColumnsShow.push(<Row key={i}> {colRowCard} </Row>)
  }

  return (
    <Container fluid>
        <BreadcrumbsSubPage separator={IconChevronRight} navigation={parameter.breadcrumb} icon title rightAlign />
        <Button icon="pi pi-backward"
                tooltip="Voltar"
                tooltipOptions={{ position: 'bottom' }}
                rounded className="p-2 mb-5"
                onClick={() => distributedFunctions.redirectPage( '../index')}/>
         <div className="card">
          {dynamicColumnsShow}
         </div>
    </Container>
  )
}
