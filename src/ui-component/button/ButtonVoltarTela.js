import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Button } from 'primereact/button';
import distributedFunctions from "../../request/distributedFunctions";

export function ButtonVoltarTela (parameter) {
    return (
        <Row style={{marginBottom: '30px'}}>
            <Col>
                <Button icon="pi pi-backward"
                        tooltip="Voltar"
                        tooltipOptions={{ position: 'bottom' }}
                        rounded className="p-2 mb-5"
                        onClick={() => distributedFunctions.redirectPage( parameter.url)}/>
            </Col>
        </Row>
    )
}