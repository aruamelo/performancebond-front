import React from 'react'
import Stack from 'react-bootstrap/Stack';
import { Button } from 'primereact/button';
import distributedFunctions from "../../request/distributedFunctions";

export function ButtonAcoesAnalisarDocumento (parameter) {
    const redirectMainPage = (page) => {
        distributedFunctions.redirectPage(page);
    }

    return (
        <Stack direction="horizontal" gap={4}>
            <Button
                icon="pi pi-eye"
                tooltip="Visualizar arquivos"
                tooltipOptions={{ position: 'bottom' }}
                rounded
                className="p-2"
                severity="primary"
                onClick={() => redirectMainPage(`${parameter.urlVisualizarRemessa}/${parameter.remessaId}`)}
            />
            <Button
                icon="pi pi-check"
                tooltip="Analisar remessa"
                tooltipOptions={{ position: 'bottom' }}
                rounded
                className="p-2"
                severity="secondary"
                onClick={() => redirectMainPage(`${parameter.urlAnalisarRemessa}/${parameter.remessaId}`)}
            />
        </Stack>
    )
}