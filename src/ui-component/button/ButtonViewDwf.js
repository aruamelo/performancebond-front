import React from 'react';
import { Button } from 'primereact/button';
import distributedFunctions from "../../request/distributedFunctions";

export function ButtonViewDwf (parameter) {
    let handleAbrirNovaGuia = () => {
        distributedFunctions.viewArquivoDwfNavegador(parameter.arquivoId)
    };

    return (
        <Button icon="pi pi-eye" tooltip="Visualizar projeto"
                onClick={handleAbrirNovaGuia} type="button"
                tooltipOptions={{position: 'bottom'}}></Button>
    )
}