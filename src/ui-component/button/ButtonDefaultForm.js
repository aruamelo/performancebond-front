import React from 'react'
import distributedFunctions from "../../request/distributedFunctions";
import { Button } from 'primereact/button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const cancelarFormulario = (url) => {
    distributedFunctions.redirectPage(url)
}

export function ButtonDefaultForm (parameter) {
    let urlCancelar = parameter.urlCancelar ??  'index'
    return (
        <Row key={'areaButton'} className={'p-4 d-flex justify-content-end'} >
            <Col key={'colButton'}>
                <Button label=" Cancelar"
                        className="p-button-rounded p-button-secondary mr-3 pi pi-ban"
                        type="button"
                        onClick={ () => cancelarFormulario(urlCancelar)}/>
                <Button label=" Salvar"
                        className="p-button-rounded pi pi-save"
                        style={{ marginLeft: '15px' }}/>
            </Col>
        </Row>
    )
}