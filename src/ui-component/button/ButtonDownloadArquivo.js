import React from 'react';
import { Button } from 'primereact/button';
import downloadArquivoService from "../../service/download/DownloadArquivoService";

export function ButtonDownloadArquivo (parameter) {
    let arquivoId = parameter.arquivo_id
    let handleDownloadClick = () => {
        downloadArquivoService.downloadArquivo(arquivoId);
    };

    return (
        <Button icon="pi pi-download" tooltip="Download do arquivo"
                onClick={handleDownloadClick} type="button" tooltipOptions={{position: 'bottom'}}></Button>
    )
}