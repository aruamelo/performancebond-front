import React, { useState, useEffect } from 'react';
import ContratoService from '../../service/contrato/ContratoService';
import { Button } from 'primereact/button'
import distributedFunctions from "../../request/distributedFunctions";
import {IndexDatatable} from "../../ui-component/index/indexDatatable";

const ContratoUser = () => {
    const [usuario, setUsuario] = useState(null);

    const user = distributedFunctions.getDataUser()

    const redirectMainPage = (idContrato)=> {
        user.user.idContrato = idContrato
        localStorage.setItem('auth', JSON.stringify(user))

        distributedFunctions.redirectPage('../../dashboard')
    }

    useEffect(() => {
        ContratoService.contratoPorUsuario(user.user.id).then((data) => {
            let dadosContrato = data.data

            dadosContrato.map((register) => {
                register.acaoBotao =
                        <Button icon="pi pi-eye"
                                tooltip="Acessar contrato"
                                tooltipOptions={{ position: 'bottom' }}
                                rounded className="p-2"
                                severity="secondary"
                                onClick= {() => redirectMainPage(register.id)}
                        />
                        }
            )
            setUsuario(dadosContrato);
        });
    }, []);

    if (!usuario) return null;

    const columns = [
        { field: 'codigo', header: 'Código' },
        { field: 'objeto', header: 'Nome' }
    ]

    return (
        <IndexDatatable columns={columns} data={usuario}
                        viewButtonDefault ={false}
                        viewButtonCreate = {false}/>
    );
};

export default ContratoUser;
