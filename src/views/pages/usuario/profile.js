import React from 'react'
import { IconChevronRight } from '@tabler/icons-react'
import Container from 'react-bootstrap/Container';
import '../../../assets/performancebond/css/perfomancebond.css'
import BreadcrumbsSubPage from "../../../ui-component/extended/BreadcrumbsSubPage";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { classNames } from 'primereact/utils';
import { useFormik } from 'formik';
import request from "../../../request/request";
import noty from "../../../alert/noty";
import distributedFunctions from "../../../request/distributedFunctions";
import { Button } from 'primereact/button';
// import usuarioDoSistemaService from "../../../service/permissoes/usuarioDoSistemaService";
import {useParams } from 'react-router-dom';

export default function Edit () {
    const { id } = useParams();

    const formik = useFormik({
        initialValues: {
            id : distributedFunctions.getDataUser().user.id,
            password: '',
            password_confirmation: ''
        },
        validate: (data) => {
            let errors = {};

            if (!data.password) {
                errors.password = 'Nome é obrigatório.';
            }

            if (!data.password_confirmation) {
                errors.password_confirmation = 'Email é obrigatório.';
            }

            if (data.password != data.password_confirmation) {
                errors.password = 'Senha digitadas estão diferentes.';
                errors.password_confirmation = 'Senha digitadas estão diferentes.';
            }

            return errors;
        },
        onSubmit: (data) => {
            request.post('usuariodosistema/alterarsenha', data).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    window.location.href='/pb'
                }, 2000)
            }).catch((err) => {
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
            });
        }
    });

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        formik.setFieldValue(name, value);
    };

    const [dadosCarregados, setDadosCarregados] = React.useState(false);

    const user = distributedFunctions.getDataUser()

    React.useEffect(() => {
        // Configurar o estado inicial do formik com os dados do usuário
        formik.setValues({
            id: user.user.id
        });
        setDadosCarregados(true);
    }, [id]);

    // Renderizar apenas quando os dados estiverem totalmente carregados
    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    const breadcrumb =  {
        "id": "perfilusuario",
        "title": "Perfil do sistema",
        "titlepage": "Alterar senha"
    }

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />
            <form onSubmit={formik.handleSubmit}>
                <div className="card">
                    <Row key={'rowFormDadosUsuario'}>
                        <Col xs={6} md={6} key={'colName'}>
                            <Row>
                                <Form.Label htmlFor="name">Nome</Form.Label>
                            </Row>
                            <Row>
                                <Form.Label htmlFor="name">{user.user.name}</Form.Label>
                            </Row>
                        </Col>
                        <Col xs={6} md={6} key={'colEmail'}>
                            <Row>
                                <Form.Label htmlFor="email">Email</Form.Label>
                            </Row>
                            <Row>
                                <Form.Label htmlFor="name">{user.user.email}</Form.Label>
                            </Row>
                        </Col>
                    </Row>
                    <Row key={'rowFormDadosSenha'}>
                        <Col xs={6} md={6} key={'colNovaSenha'}>
                            <Form.Label htmlFor="inputNome">Nova senha</Form.Label>
                            <Form.Control
                                type="password"
                                id="password"
                                name="password"
                                value={formik.values.password}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar a nova senha"
                                onChange={handleInputChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('password') })}
                            />
                            {getFormErrorMessage('password')}
                        </Col>
                        <Col xs={6} md={6} key={'colRepetirSenha'}>
                            <Form.Label htmlFor="inputNome">Nova senha</Form.Label>
                            <Form.Control
                                type="password"
                                id="password_confirmation"
                                name="password_confirmation"
                                value={formik.values.password_new}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Repetir a nova senha"
                                onChange={handleInputChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('password_confirmation') })}
                            />
                            {getFormErrorMessage('password_new')}
                        </Col>
                    </Row>
                </div>

                <Row key={'areaButton'} className={'p-4'} >
                    <Col key={'colButton'}>
                        <Button label=" Salvar" className="p-button-rounded pi pi-save"/>
                    </Col>
                </Row>
            </form>

        </Container>

    )
}
