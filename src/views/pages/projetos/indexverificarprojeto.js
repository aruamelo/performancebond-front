import React from 'react'
import { IndexDatatable } from '../../../ui-component/index/indexDatatable'
import projetosService from "../../../service/projetos/ProjetosService";
import distributedFunctions from "../../../request/distributedFunctions";
import {ButtonAcoesAnalisarDocumento} from "../../../ui-component/button/ButtonAcoesAnalisarDocumento";

export default function Index () {
    const [listaDocumentoSubmetido, setListaDocumentoSubmetido] = React.useState(null);

    React.useEffect(() => {
        const contratoId = distributedFunctions.getIdContratoUser()
        projetosService.getListaDocumentosSubmetidosVerificador(contratoId).then(data => {
            let remessaSubmetida = data.data;

            remessaSubmetida.map((item) => {
                item.acaoBotao = (
                                    <ButtonAcoesAnalisarDocumento
                                        urlVisualizarRemessa={'visualizar_remessa_arquivo'}
                                        urlAnalisarRemessa={'analisar_remessa_arquivo_verificador'}
                                        remessaId ={item.id}
                                    />
                                )
            })

            setListaDocumentoSubmetido(remessaSubmetida)
        })
    }, [])

    if (!listaDocumentoSubmetido) return null;

    const columns = [
        { field: 'sequencial', header: 'Nº da remessa' },
        { field: 'data_ultima_alteracao', header: 'Data envio análise a' },
        { field: 'quantidade_documento', header: 'Quantidade arquivo remessa' }
    ]

    return (
        <IndexDatatable columns={columns} data={listaDocumentoSubmetido}
                        viewButtonDefault ={false}
                        viewButtonCreate = {false}/>
    )
}
