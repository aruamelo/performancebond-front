import React from 'react';
import { IndexDatatable } from '../../../ui-component/index/indexDatatable';
import remessaDeArquivoService from "../../../service/remessadearquivo/RemessaDeArquivoService";
import Stack from 'react-bootstrap/Stack';
import { useParams } from 'react-router-dom';
import BreadcrumbsSubPage from "../../../ui-component/extended/BreadcrumbsSubPage";
import { IconChevronRight } from '@tabler/icons-react'
import {ButtonViewDwf} from "../../../ui-component/button/ButtonViewDwf";
import {ButtonDownloadArquivo} from "../../../ui-component/button/ButtonDownloadArquivo";
import {ButtonVoltarTela} from "../../../ui-component/button/ButtonVoltarTela";

export default function Index () {
    const [remessaSubmetida, setRemessaSubmetida] = React.useState(null);
    const [sequencial, setSequencial] = React.useState(null);
    const { remessa_id } = useParams();
    const exibirRemessaSubmetida = () => {
        remessaDeArquivoService.showRemessaDocumentoSubmetido(remessa_id).then(data => {
            let dadosRemessa = data.data

            let visualizarArquivos = dadosRemessa.arquivos.map((item) => {
                item.acaoBotao = (
                    <Stack direction="horizontal" gap={4}>
                        <ButtonDownloadArquivo arquivo_id = {item.arquivo_id} />
                        {item.tipo_arquivo === 'dwf' && (
                            <ButtonViewDwf arquivoId={item.arquivo_id}/>
                        )}
                    </Stack>
                );

                return item;
            });
            setRemessaSubmetida(visualizarArquivos);
            setSequencial(dadosRemessa.sequencial)
        });
    };

    React.useEffect(() => {
        exibirRemessaSubmetida();
    }, []);

    if (!remessaSubmetida) return null;

    const columns = [
        { field: 'nome_documento', header: 'Nome documento' },
        { field: 'titulo_documento', header: 'Título do documento' }
    ];

    const breadcrumb =  {
        "id": "trecho",
        "title": "Aprovar documento\n",
        "titlepage": `Visualizar documento(s) da remessa ${sequencial}`,
        "permission": "remessa_de_arquivo__analisar_remessa_submetida__aprovador"
    }

    return (
        <>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />

            <ButtonVoltarTela url={'../aprovar'} />

            <IndexDatatable columns={columns} data={remessaSubmetida}
                            viewButtonDefault ={false}
                            viewButtonCreate = {false}/>
        </>
    );
}
