
import React, { useState, useEffect } from 'react';
import { Button } from 'primereact/button';
import {useParams } from 'react-router-dom';
import { InputTextarea } from 'primereact/inputtextarea';
import BreadcrumbsSubPage from "../../../ui-component/extended/BreadcrumbsSubPage";
import Container from 'react-bootstrap/Container';
import { IconChevronRight } from '@tabler/icons-react'
import distributedFunctions from "../../../request/distributedFunctions";
import noty from "../../../alert/noty";
import { useFormik } from 'formik';
import {DataScrollerCustom} from "../../../ui-component/datascroller/DataScrollerCustom";
import {ButtonDownloadArquivo} from "../../../ui-component/button/ButtonDownloadArquivo";
import projetosService from "../../../service/projetos/ProjetosService";
import { RadioButton } from 'primereact/radiobutton';
import {ButtonViewDwf} from "../../../ui-component/button/ButtonViewDwf";
import {ButtonVoltarTela} from "../../../ui-component/button/ButtonVoltarTela";
import LoadingPerformancebond from "../../../ui-component/loading/LoadingPerformancebond";

export default function DataScrollerInlineDemo () {
    const [products, setProducts] = useState([]);
    const [dadosCarregados, setDadosCarregados] = React.useState(false);
    const [rascunhoAnaliseAtualizados, setRascunhoAnaliseAtualizados] = useState({});
    const [analiseArquivo, setAnaliseArquivo] = useState([]);
    const [selectedAnalise, setSelectedAnalise] = useState({}); // Estado para armazenar os valores selecionados
    const [loading, setLoading] = useState(false);

    const { remessa_id } = useParams();

    const formik = useFormik({
        initialValues: {
            titulo_documento: []
        },
        onSubmit: () => {
            setLoading(true)
            const qtdItemAnalisado = Object.keys(selectedAnalise).length;

            if (qtdItemAnalisado < products.arquivos.length) {
                noty.warning('Para finalizar a análise, todos os documentos devem ter uma situação selecionada')
                setLoading(false)
                return
            }

            let dadosRascunho = projetosService.formatarDadosEnviar(
                products.arquivos,
                remessa_id,
                selectedAnalise,
                rascunhoAnaliseAtualizados
            );

            projetosService.finalizarAnaliseAprovador(dadosRascunho).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    distributedFunctions.redirectPage('../aprovar')
                }, 2000)
            }).catch((err) => {
                if (err.response.status === 401) {
                    window.location.href = '/pb'
                }
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    useEffect(() => {
        projetosService.getDocumentosRemessaParaAprovar(remessa_id).then((data) => {
            let dados = data.data
            setProducts(dados)
            const initialSelectedAnalise = {};
            const initialRascunhoAnaliseAtualizados = {};

            dados.arquivos.forEach(arquivo => {
                // Preencher valores iniciais para os radio buttons
                if (arquivo.situacao_id) {
                    initialSelectedAnalise[arquivo.arquivo_id] = arquivo.situacao_id;
                }

                // Preencher valores iniciais para os textareas
                if (arquivo.justificativa) {
                    initialRascunhoAnaliseAtualizados[arquivo.arquivo_id] = arquivo.justificativa;
                }
            });

            setSelectedAnalise(initialSelectedAnalise)
            setRascunhoAnaliseAtualizados(initialRascunhoAnaliseAtualizados)
        });

        projetosService.getSituacaoAnalise().then((data) => {
            setAnaliseArquivo(data.data)
            setDadosCarregados(true);
        })
    }, []);

    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    const tituloGrid = `Remessa nº ${products.sequencial}`

    const breadcrumb =  {
        "id": "entradadedocumento",
        "title": "Aprovar remessa de arquivo",
        "titlepage": tituloGrid,
        "permission": "remessa_de_arquivo__analisar_remessa_submetida__aprovador"
    }

    const handleSalvarRascunhoAnalise = () => {
        setLoading(true)
        if (distributedFunctions.isEmptyObject(rascunhoAnaliseAtualizados) &&
            distributedFunctions.isEmptyObject(selectedAnalise)
        ) {
            noty.warning('Informe ao menos uma análise ou justificativa para salvar o rascunho')
            setLoading(false)
            return
        }

        let dadosRascunho = projetosService.formatarDadosEnviar(
            products.arquivos,
            remessa_id,
            selectedAnalise,
            rascunhoAnaliseAtualizados
        );

        projetosService.salvarRascunhoAnaliseAprovador(dadosRascunho).then(data => {
            noty.success(data.data.message);
            setLoading(false)
        });
        setLoading(false)
    };


    const onAnaliseChange = (e, arquivoId) => {
        const selected = { ...selectedAnalise };
        selected[arquivoId] = e.value; // Atualiza o valor selecionado para o arquivo específico
        setSelectedAnalise(selected);
    };

    const itemTemplate = (data) => {
        let buttonViewDwf = null

        if (data.tipo_arquivo == 'dwf') {
            buttonViewDwf = <ButtonViewDwf arquivoId={data.arquivo_id} />
        }

        let handleAtualizarTitulo = (arquivoId, novoTitulo) => {
            if (novoTitulo.trim() === '') {
                const newTitulosAtualizados = {...rascunhoAnaliseAtualizados};
                delete newTitulosAtualizados[arquivoId];
                setRascunhoAnaliseAtualizados(newTitulosAtualizados);
                return
            }

            setRascunhoAnaliseAtualizados(prevState => ({
                ...prevState,
                [arquivoId]: novoTitulo
            }));
        };

        const radioButtonAnalise = Object.entries(analiseArquivo).map(([key, value]) => {
            const idRadio = `rb_analise_${key}_${data.arquivo_id}`;
            return (
                <div key={idRadio} className="field-radiobutton">
                    <RadioButton
                        inputId={idRadio}
                        value={key}
                        onChange={(e) => onAnaliseChange(e, data.arquivo_id)}
                        checked={selectedAnalise[data.arquivo_id] == key}
                    />
                    <label htmlFor={idRadio}>{value}</label>
                </div>
            );
        });

        return (
            <div className="product-item">
                <div className="product-detail">
                    <div className="product-name">{data.nome_documento}</div>
                    <div className="product-description" >{data.titulo_documento}</div>
                    <div className="product-description">
                        <label>Situação da análise</label> <br/>
                        {radioButtonAnalise}

                        <label className={'mt-3'}>Justificativa da análise</label> <br/>
                        <InputTextarea
                            rows={5}
                            cols={80}
                            autoResize
                            placeholder="Inserir a justificativa para o documento"
                            value={rascunhoAnaliseAtualizados[data.arquivo_id] || ''}
                            onChange={(e) => handleAtualizarTitulo(data.arquivo_id, e.target.value)}
                            onInput={(e) => handleAtualizarTitulo(data.arquivo_id, e.target.value)}
                        />
                    </div>
                </div>
                <div className="product-action">
                    <ButtonDownloadArquivo arquivo_id={data.arquivo_id}/>
                    {buttonViewDwf}
                </div>
            </div>
        );
    }

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign/>

            <ButtonVoltarTela url={'../aprovar'} />

            <form onSubmit={formik.handleSubmit}>

                <DataScrollerCustom rows={products.arquivos} itemTemplate={itemTemplate} header={tituloGrid} />

                <Button label=" Salvar rascunho"
                        className="p-button-rounded pi pi-save"
                        style={{ marginLeft: '15px', marginTop: '10px' }}
                        type="button"
                        onClick={handleSalvarRascunhoAnalise}/>

                <Button label=" Finalizar análise"
                        className="p-button-rounded pi pi-save"
                        style={{ marginLeft: '15px', marginTop: '10px' }}/>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>
    );
}
                 