import React from 'react';
import projetosService from "../../../service/projetos/ProjetosService";
import distributedFunctions from "../../../request/distributedFunctions";
import { Tree } from 'primereact/tree';
import downloadArquivoService from "../../../service/download/DownloadArquivoService";

export default function Index() {
    const [nodes, setNodes] = React.useState(null);
    const [loading, setLoading] = React.useState(true);
    const [fonte, setFonte] = React.useState([]);

    const contratoId = distributedFunctions.getIdContratoUser()

    const loadOnExpand = (event) => {
        if (!event.node.children) {
            let fonteAnterior = {...fonte};
            let arrayFonte = {};

            fonteAnterior = Object.fromEntries(
                /* eslint-disable-next-line */
                Object.entries(fonteAnterior).filter(([key, value]) => value !== undefined)
            );

            let keyNivel = parseInt(event.node.key.split('-'));

            let fonteAnteriorObject = fonteAnterior[keyNivel]

            let keyFormatada = event.node.key

            // Caso exista valor selecionado, então formatada a key para o último nó selecionado
            if (keyFormatada.length > 1) {
                let arrayKey = event.node.key.split('-')
                arrayKey.pop()
                keyFormatada = arrayKey.join('-')
            }

            let objetoVazio  = distributedFunctions.isEmptyObject(fonteAnteriorObject)

            if (!objetoVazio) {
                fonteAnteriorObject = fonteAnteriorObject[keyFormatada]
            }

            Object.entries(fonteAnteriorObject).forEach(([chave, valor]) => {
                arrayFonte[chave] = valor;
            });

            arrayFonte[event.node.fonte] = event.node.data;

            let posicaoAtual = event.node.posicao + 1;

            let dataProximoNo = {
                contrato_id: contratoId,
                posicao: posicaoAtual,
                fonte: arrayFonte
            };

            fonteAnterior[keyNivel] = { ...fonteAnterior[keyNivel], [event.node.key]: arrayFonte };

            setFonte(fonteAnterior);

            projetosService.listagemDocumentoProximoNo(dataProximoNo).then(data => {
                let noRecuperado = data.data;
                let node = { ...event.node };
                node.children = noRecuperado;

                let novaStr = Object.values(arrayFonte).join('-');
                let foundNode = findObjectRecursive(nodes, 'key', novaStr);

                if (foundNode !== null) {
                    // Atualiza o objeto encontrado com os dados recuperados
                    foundNode.children = noRecuperado;

                    // Atualiza o estado nodes com o novo objeto encontrado
                    setNodes(prevNodes => {
                        return prevNodes.map(prevNode => prevNode.key === foundNode.key ? foundNode : prevNode);
                    });

                    setLoading(false);
                }
            });
        }
    }


    function findObjectRecursive(array, key, value) {
        for (let i = 0; i < array.length; i++) {
            const obj = array[i];
            if (obj[key] === value) {
                obj.children = []; // Adiciona uma propriedade children vazia ao objeto encontrado
                return obj;
            }
            if (obj.children) {
                const objInChildren = findObjectRecursive(obj.children, key, value);
                if (objInChildren !== null) {
                    return objInChildren;
                }
            }
        }
        return null;
    }

    React.useEffect(() => {
        projetosService.listagemDocumentoNivelUm(contratoId).then(data => {
            let arrayFonte = [];

            let remessaSubmetida = data.data.map((item) => {
                arrayFonte[item.id] = {}
                return {
                    key: `${item.id}`,
                    label: `${item.descricao} (${item.qtd_documento})`,
                    posicao: 1,
                    data: item.id,
                    fonte: item.fonte,
                    leaf: false
                }
            });

            setNodes(remessaSubmetida)
            setLoading(false)
            setFonte(arrayFonte)
        })
    }, [])

    const handleNodeDoubleClick = async (event) => {

        if (event.node.fonte == 'arquivo' && event.node.label == 'Download') {
            downloadArquivoService.downloadArquivo(event.node.data);
        }

        if (event.node.fonte == 'arquivo' && event.node.label == 'Visualizar') {
            distributedFunctions.viewArquivoDwfNavegador(event.node.data)
        }
    };

    return (
        <Tree value={nodes} onExpand={loadOnExpand} loading={loading} onNodeDoubleClick={handleNodeDoubleClick} />

    )
}
