import React from 'react'
import Container from 'react-bootstrap/Container';
import '../../../assets/performancebond/css/perfomancebond.css'
import { FileUpload } from 'primereact/fileupload';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { useFormik } from 'formik';
import remessaDeArquivoService from "../../../service/remessadearquivo/RemessaDeArquivoService";
import noty from "../../../alert/noty";
import distributedFunctions from "../../../request/distributedFunctions";
import { Button } from 'primereact/button';
import { Message } from 'primereact/message'; // Import the Message component
import LoadingPerformancebond from "../../../ui-component/loading/LoadingPerformancebond";

export default function Index () {
    const [documentoSubmetido, setDocumentoSubmetido] = React.useState(null);
    const [dadosCarregados, setDadosCarregados] = React.useState(false);
    const [validationFile, setValidationFile] = React.useState(false);
    const [loading, setLoading] = React.useState(false);

    const formik = useFormik({
        initialValues: {
            contrato_id: distributedFunctions.getIdContratoUser(),
            file: []
        },
        validate: (data) => {
            let errors = {};

            if (data.file.length == 0) {
                errors.file = 'Selecionar ao menos um arquivo para submeter a remessa'
            }

            return errors;
        },
        onSubmit: (data) => {
            setLoading(true)
            let formData = new FormData();

            for (const file of data.file) {
                let blob = new Blob([file], { type: file.type });
                formData.append('file[]', blob, file.name);
            }

            const contrato_id = distributedFunctions.getIdContratoUser();
            formData.append('contrato_id', contrato_id);

            remessaDeArquivoService.submeterDocumento(formData).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    let remessa_id = data.data.data.id_remessa
                    distributedFunctions.redirectPage(`remessa_arquivo/${remessa_id}`)
                }, 2000)
            }).catch((err) => {
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    React.useEffect(() => {
        const contrato_id = distributedFunctions.getIdContratoUser()
        remessaDeArquivoService.getCampos(contrato_id).then((data) => {
            setDocumentoSubmetido(data.data)
            setDadosCarregados(true);
        });
    }, []);

    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const quantidadeTotalCampoShow = documentoSubmetido[0].campos.length
    const quantidadeColunaPorLinha =  4
    const quantidadeRow = Math.ceil(quantidadeTotalCampoShow / quantidadeColunaPorLinha, 1)

    // Separar o array para poder incluir conforme a row e col
    let arrayColumnRow = []
    for (let i = 0; i < quantidadeTotalCampoShow; i += quantidadeColunaPorLinha) {
        arrayColumnRow.push(documentoSubmetido[0].campos.slice(i, i + quantidadeColunaPorLinha));
    }

    let dynamicColumnsShow = []
    // Realizar o laço para incluir a quantidade de linhas com base na quantidade de campos e por colunas em sua linha
    for (let i = 0; i < quantidadeRow; i++) {
        // Percorrer o array dividido com base na quantidade de linhas
        let colRowCard = arrayColumnRow[i].map((dataColumn) => {
            return <Col key={dataColumn.id}>
                <div className="pt-3">
                    <label htmlFor={dataColumn.id} >{dataColumn.texto_exemplo}</label>
                </div>
                <div className="pt-3">
                    {dataColumn.hint}
                </div>
            </Col>
        })
        // Incluir as colunas em cada linha
        dynamicColumnsShow.push(<Row key={i}> {colRowCard} </Row>)
    }
    const chooseOptions = {label: 'Selecionar', icon: 'pi pi-fw pi-plus'};
    const uploadOptions = {style: {display: 'none'} };
    const cancelOptions = {style: {display: 'none'} };

    const validationFileSubmit = async (e) => {
        setValidationFile(null);

        const files = e.files;
        let dataErro = [];
        let arquivosValidados = []
        for (const file of files) {
            let formData = formSendFile(file, 'arquivo')
            try {
                await remessaDeArquivoService.validarNomeArquivo(formData);

                arquivosValidados.push(file)
                // setFile(file)

            } catch (error) {
                let responseErro = error.response.data.message ?? error.response.data.arquivo
                let message = `Arquivo ${file.name}, com o erro: ${responseErro}.`;

                dataErro.push(<Message severity="error" text={message} />);
            }
        }

        if (dataErro.length > 0) {
            setValidationFile(dataErro);
            return
        }

        formik.setFieldValue('file', arquivosValidados);
    };

    function formSendFile(file, nomeCampo) {
        let formData = new FormData();
        const blob = new Blob([file], { type: file.type });
        const contrato_id = distributedFunctions.getIdContratoUser();

        formData.append(nomeCampo, blob, file.name);
        formData.append('contrato_id', contrato_id);

        return formData
    }

    return (
        <Container fluid>
            <form onSubmit={formik.handleSubmit} >
                <div className="card">
                    <Row key={'rowDescricaoCampo'}>
                        <Col xs={6} md={12} key={'colDescricaoCampo'}>
                            Descrição do(s) campo(s)
                        </Col>
                    </Row>

                    {dynamicColumnsShow}

                    <Row key={'rowEAP'} className="mt-5">
                        <Col xs={6} md={12} key={'colEAP'}>
                            EAP base <b>{documentoSubmetido[0].eap_exibicao}</b>
                        </Col>
                    </Row>

                    {validationFile && validationFile.length > 0 && (
                        <Row key={'rowMessageError'}>
                            <label className="p-field mt-3" style={{color: 'red'}}>
                                Erro ao processar o(s) arquivo(s), favor remover e inserir novamente
                            </label>

                            {validationFile.map((error, index) => (
                                <div className="p-field mt-3" key={index}>
                                    <React.Fragment key={index}>{error}</React.Fragment>
                                </div>
                            ))}
                        </Row>
                    )}

                    <div className="p-field mt-5">
                        {getFormErrorMessage('file')}
                        <FileUpload
                            onSelect = {validationFileSubmit}
                            chooseOptions={chooseOptions}
                            uploadOptions = {uploadOptions}
                            cancelOptions = {cancelOptions}
                            mode="advanced"
                            name="arquivos[]"
                            // url="/upload"
                            accept=".pdf, .dwf"
                            maxFileSize={10000000}
                            multiple
                            emptyTemplate="Selecionar ou arrastar o(s) arquivo(s) para enviar"
                        />
                    </div>
                </div>

                <Row key={'areaButton'} className={'p-4 d-flex justify-content-end'} >
                    <Col key={'colButton'}>
                        <Button label=" Próximo"
                                className="p-button-rounded pi pi-save"
                                style={{ marginLeft: '15px' }}
                                type='submit'/>
                    </Col>
                </Row>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>

    )
}
