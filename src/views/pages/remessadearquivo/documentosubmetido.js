import React from 'react';
import { IndexDatatable } from '../../../ui-component/index/indexDatatable';
import remessaDeArquivoService from "../../../service/remessadearquivo/RemessaDeArquivoService";
import distributedFunctions from "../../../request/distributedFunctions";
import { Button } from 'primereact/button';
import Stack from 'react-bootstrap/Stack';
import noty from "../../../alert/noty";
import { confirmDialog } from 'primereact/confirmdialog';
export default function Index () {
    const [remessaSubmetida, setRemessaSubmetida] = React.useState(null);

    const redirectMainPage = (page) => {
        distributedFunctions.redirectPage(page);
    }

    const cancelarRemessa = (id) => {
        remessaDeArquivoService.cancelarRemessa(id).then(data => {
            noty.success(data.data.message);

            setTimeout(function () {
                window.location.reload()
            }, 2000)
        }).catch((err) => {
            if (err.response.status === 401) {
                window.location.href = '/pb';
            }
            let mensagem = distributedFunctions.handleErrorMessage(err);
            noty.error(mensagem);
        })
    };

    const finalizarRemessa = (id) => {
        remessaDeArquivoService.finalizarRemessaDocumentoSubmetido(id).then(data => {
            noty.success(data.data.message);

            setTimeout(function () {
                window.location.reload()
            }, 2000)
        }).catch((err) => {
            if (err.response.status === 401) {
                window.location.href = '/pb';
            }
            let mensagem = distributedFunctions.handleErrorMessage(err);
            noty.error(mensagem);
        })
    };

    const dialogCancelarRemessa = (remessaId, numeroRemessa) => {
        confirmDialog({
            message: `Deseja cancelar a remessa de documento nº ${numeroRemessa}?`,
            header: 'Cancelar remessa de arquivo',
            icon: 'pi pi-info-circle',
            acceptLabel: 'Sim',
            rejectLabel: 'Não',
            accept: () => cancelarRemessa(remessaId)
        });
    };

    const dialogFinalizarRemessa = (remessaId, numeroRemessa) => {
        confirmDialog({
            message: `Deseja finalizar a remessa de documento nº ${numeroRemessa}?`,
            header: 'Finalizar remessa de arquivo',
            icon: 'pi pi-info-circle',
            acceptLabel: 'Sim',
            rejectLabel: 'Não',
            accept: () => finalizarRemessa(remessaId)
        });
    };

    const exibirRemessaSubmetida = () => {
        const contratoId = distributedFunctions.getIdContratoUser();
        remessaDeArquivoService.getRemessasArquivo(contratoId).then(data => {
            let remessaSubmetida = data.data;
            remessaSubmetida.map((item) => {
                item.acaoBotao = (
                    <Stack direction="horizontal" gap={4}>
                        <Button
                            icon="pi pi-eye"
                            tooltip="Visualizar arquivos"
                            tooltipOptions={{ position: 'bottom' }}
                            rounded
                            className="p-2"
                            severity="primary"
                            onClick={() => redirectMainPage(`visualizar_remessa_arquivo/${item.id}`)}
                        />
                        {item.situacaoRemessa === 'Em elaboração' && (
                            <>
                                <Button
                                    icon="pi pi-check"
                                    tooltip="Submeter remessa"
                                    tooltipOptions={{ position: 'bottom' }}
                                    rounded
                                    className="p-2"
                                    severity="primary"
                                    onClick={() => dialogFinalizarRemessa(item.id, item.numeroRemessa)}
                                />
                                <Button
                                    icon="pi pi-file-edit"
                                    tooltip="Editar remessa"
                                    tooltipOptions={{ position: 'bottom' }}
                                    rounded
                                    className="p-2"
                                    severity="secondary"
                                    onClick={() => redirectMainPage(`remessa_arquivo/${item.id}`)}
                                />
                                <Button
                                    icon="pi pi-ban"
                                    tooltip="Cancelar remessa"
                                    tooltipOptions={{ position: 'bottom' }}
                                    rounded
                                    className="p-2"
                                    severity="danger"
                                    onClick={() => {
                                        dialogCancelarRemessa(item.id, item.numeroRemessa);
                                    }}
                                />
                            </>
                        )}
                    </Stack>
                );
            });
            setRemessaSubmetida(remessaSubmetida);
        });
    };

    React.useEffect(() => {
        exibirRemessaSubmetida();
    }, []);

    if (!remessaSubmetida) return null;

    const columns = [
        { field: 'numeroRemessa', header: 'Nº remessa' },
        { field: 'situacaoRemessa', header: 'Situação' },
        { field: 'dataSolicitacao', header: 'Data da solicitação' },
        { field: 'dataUltimaModificacao', header: 'Data última modificação' },
        { field: 'solicitante', header: 'Solicitante' },
    ];

    return (
        <>
            <IndexDatatable columns={columns} data={remessaSubmetida}
                            viewButtonDefault ={false}
                            viewButtonCreate = {false}/>
        </>
    );
}
