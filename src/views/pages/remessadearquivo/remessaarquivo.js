
import React, { useState, useEffect } from 'react';
import { Button } from 'primereact/button';
import remessaDeArquivoService from "../../../service/remessadearquivo/RemessaDeArquivoService";
import {useParams } from 'react-router-dom';
import { InputTextarea } from 'primereact/inputtextarea';
import BreadcrumbsSubPage from "../../../ui-component/extended/BreadcrumbsSubPage";
import Container from 'react-bootstrap/Container';
import { IconChevronRight } from '@tabler/icons-react'
import distributedFunctions from "../../../request/distributedFunctions";
import noty from "../../../alert/noty";
import { useFormik } from 'formik';
import $ from 'jquery';
import {DataScrollerCustom} from "../../../ui-component/datascroller/DataScrollerCustom";
import {ButtonDownloadArquivo} from "../../../ui-component/button/ButtonDownloadArquivo";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {ButtonViewDwf} from "../../../ui-component/button/ButtonViewDwf";
import LoadingPerformancebond from "../../../ui-component/loading/LoadingPerformancebond";

export default function DataScrollerInlineDemo () {
    const [products, setProducts] = useState([]);
    const [dadosCarregados, setDadosCarregados] = React.useState(false);
    const [titulosAtualizados, setTitulosAtualizados] = useState({});
    const [loading, setLoading] = React.useState(false);

    const { remessa_id } = useParams();

    const formik = useFormik({
        initialValues: {
            titulo_documento: []
        },
        onSubmit: (data) => {
            setLoading(true)
            data = {titulos_atualizados: titulosAtualizados, remessa_id}
            remessaDeArquivoService.finalizarRemessa(data).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    distributedFunctions.redirectPage('../index')
                }, 2000)
            }).catch((err) => {
                if (err.response.status === 401) {
                    window.location.href = '/pb'
                }
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    useEffect(() => {
        remessaDeArquivoService.showRemessaArquivo(remessa_id).then((data) => {
            setProducts(data.data)
            setDadosCarregados(true);

            // Extrair os títulos dos arquivos e atualizar o estado titulosAtualizados
            const novosTitulosAtualizados = {};
            data.data.arquivos.forEach(arquivo => {
                novosTitulosAtualizados[arquivo.arquivo_id] = arquivo.titulo_documento;
            });
            setTitulosAtualizados(novosTitulosAtualizados);
        });
    }, []);

    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    const tituloGrid = `Remessa nº ${products.sequencial}`

    const breadcrumb =  {
        "id": "entradadedocumento",
        "title": "Remessa de arquivo",
        "titlepage": tituloGrid,
        "permission": "remessa_de_arquivo__submeter_arquivo"
    }

    const handleSalvarTitulos = () => {
        setLoading(true)
        const titulos = Object.entries(titulosAtualizados).map(([arquivoId, titulo]) => ({
            arquivoId,
            titulo
        }));

        remessaDeArquivoService.atualizarTitulo(titulos).then(data => {

            noty.success(data.data.message)
            let titulosComSucesso = data.data.data
            titulosComSucesso.forEach(function(dadosArquivo) {
                $('#label_titulo_' + dadosArquivo.arquivoId).text(dadosArquivo.titulo);
            })
            setLoading(false)
        })
        setLoading(false)
    };


    const itemTemplate = (data) => {
        let buttonViewDwf = null

        if (data.tipo_arquivo == 'dwf') {
            buttonViewDwf = <ButtonViewDwf arquivoId={data.arquivo_id} />
        }

        let handleAtualizarTitulo = (arquivoId, novoTitulo) => {
            if (novoTitulo.trim() === '') {
                const newTitulosAtualizados = { ...titulosAtualizados };
                delete newTitulosAtualizados[arquivoId];
                setTitulosAtualizados(newTitulosAtualizados);
                return
            }

            setTitulosAtualizados(prevState => ({
                ...prevState,
                [arquivoId]: novoTitulo
            }));
        };


        return (
            <div className="product-item">
                <div className="product-detail">
                    <div className="product-name">{data.nome_documento}</div>
                    <div className="product-description"
                         id={'label_titulo_' + data.arquivo_id}>{data.titulo_documento}</div>
                    <div className="product-description">
                        <label>Título do documento</label> <br/>
                        <InputTextarea rows={5}
                                       cols={80}
                                       value={titulosAtualizados[data.arquivo_id] || data.titulo_documento}
                                       autoResize
                                       placeholder="Inserir o título para o documento"
                                       onChange={(e) => handleAtualizarTitulo(data.arquivo_id, e.target.value)}
                        />
                    </div>
                </div>
                <div className="product-action">
                    <ButtonDownloadArquivo arquivo_id={data.arquivo_id} />
                    {buttonViewDwf}
                </div>
            </div>
        );
    }

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />

            <Row style={{marginBottom: '30px'}}>
                <Col>
                    <Button icon="pi pi-backward"
                            tooltip="Voltar"
                            tooltipOptions={{ position: 'bottom' }}
                            rounded className="p-2"
                            onClick={() => distributedFunctions.redirectPage( '../documentosubmetido')}/>
                </Col>
            </Row>

            <form onSubmit={formik.handleSubmit}>

                <DataScrollerCustom rows={products.arquivos} itemTemplate={itemTemplate} header={tituloGrid} />

                <Button label=" Salvar rascunho"
                        className="p-button-rounded pi pi-save"
                        style={{ marginLeft: '15px', marginTop: '10px' }}
                        type="button"
                        onClick={handleSalvarTitulos}/>

                <Button label=" Finalizar remessa"
                        className="p-button-rounded pi pi-save"
                        style={{ marginLeft: '15px', marginTop: '10px' }}/>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>
    );
}
                 