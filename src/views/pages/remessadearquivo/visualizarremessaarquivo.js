import React from 'react';
import { IndexDatatable } from '../../../ui-component/index/indexDatatable';
import remessaDeArquivoService from "../../../service/remessadearquivo/RemessaDeArquivoService";
import { Button } from 'primereact/button';
import Stack from 'react-bootstrap/Stack';
import { useParams } from 'react-router-dom';
import distributedFunctions from "../../../request/distributedFunctions";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import BreadcrumbsSubPage from "../../../ui-component/extended/BreadcrumbsSubPage";
import { IconChevronRight } from '@tabler/icons-react'
import {ButtonDownloadArquivo} from "../../../ui-component/button/ButtonDownloadArquivo";
import {ButtonViewDwf} from "../../../ui-component/button/ButtonViewDwf";

export default function Index () {
    const [remessaSubmetida, setRemessaSubmetida] = React.useState(null);
    const { remessa_id } = useParams();

    const exibirRemessaSubmetida = () => {
        remessaDeArquivoService.showRemessaDocumentoSubmetido(remessa_id).then(data => {
            let visualizarArquivos = data.data.arquivos.map((item) => {
                item.acaoBotao = (
                    <Stack direction="horizontal" gap={4}>
                        <ButtonDownloadArquivo arquivo_id = {item.arquivo_id} />
                        {item.tipo_arquivo === 'dwf' && (
                            <ButtonViewDwf arquivoId={item.arquivo_id}/>
                        )}
                    </Stack>
                );

                return item;
            });
            setRemessaSubmetida(visualizarArquivos);
        });
    };

    React.useEffect(() => {
        exibirRemessaSubmetida();
    }, []);

    if (!remessaSubmetida) return null;

    const columns = [
        { field: 'nome_documento', header: 'Nome documento' },
        { field: 'titulo_documento', header: 'Título do documento' }
    ];

    const breadcrumb =  {
        "id": "trecho",
        "title": "Documento Submetido\n",
        "titlepage": "Visualizar documento(s)",
        "permission": "remessa_de_arquivo__submeter_arquivo"
    }
    return (
        <>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />

            <Row style={{marginBottom: '30px'}}>
                <Col>
                    <Button icon="pi pi-backward"
                            tooltip="Voltar"
                            tooltipOptions={{ position: 'bottom' }}
                            rounded className="p-2 mb-5"
                            onClick={() => distributedFunctions.redirectPage( '../documentosubmetido')}/>
                </Col>
            </Row>

            <IndexDatatable columns={columns} data={remessaSubmetida}
                            viewButtonDefault ={false}
                            viewButtonCreate = {false}/>
        </>
    );
}
