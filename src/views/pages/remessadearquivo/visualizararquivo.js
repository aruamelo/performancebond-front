import React from 'react'
import Container from 'react-bootstrap/Container';
import '../../../assets/performancebond/css/perfomancebond.css'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import remessaDeArquivoService from "../../../service/remessadearquivo/RemessaDeArquivoService";
import { Button } from 'primereact/button';
import {useParams } from 'react-router-dom';
import autodeskService from "../../../service/autodesk/AutodeskService";
import downloadArquivoService from "../../../service/download/DownloadArquivoService";

export default function Index () {
    const { arquivo_id } = useParams();

    const [dadosCarregados, setDadosCarregados] = React.useState(false);

    React.useEffect(() => {
        remessaDeArquivoService.viewdwf(arquivo_id).then((data) => {
            autodeskService.initializeViewerOffline(data.data)

            setDadosCarregados(true);
        });
    }, []);
    const handleDownloadClick = () => {
        downloadArquivoService.downloadArquivo(arquivo_id);
    };

    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    return (
        <Container fluid>
            <form>
                <i className="pi pi-download" style={{'fontSize': '2em'}} title="Download do arquivo"
                   onClick={handleDownloadClick}></i>

                <div id="viewer-container" style={{ marginTop:'50px'}}/>

                <Row key={'areaButton'} className={'p-4 d-flex justify-content-end'}>
                    <Col key={'colButton'}>
                        <Button label=" Próximo"
                                className="p-button-rounded pi pi-save"
                                style={{marginLeft: '15px'}}
                                type='submit'/>
                    </Col>
                </Row>
            </form>

        </Container>

    )
}
