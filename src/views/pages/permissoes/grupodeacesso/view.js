import React from 'react'
import { useParams } from "react-router-dom";
import grupoDeAcessoService from "../../../../service/permissoes/GrupoDeAcessoService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewGrupoDeAcesso () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [grupoDeAcesso, setGrupoDeAcesso] = React.useState(null);

  React.useEffect(() => {
    grupoDeAcessoService.showGrupoDeAcessoService(id).then(data => {
      setGrupoDeAcesso(data.data)
    })
  }, [])

  if (!grupoDeAcesso) return null;

  const show = [
    { field: 'name', label: 'Nome', value: grupoDeAcesso.name },
    { field: 'created_at', label: 'Data de criação', value: grupoDeAcesso.created_at }
  ]
  const breadcrumb =  {
    "id": "grupodeacesso",
    "title": "Grupo de acesso do sistema",
    "titlepage": "Informações do grupo de acesso",
    "permission": "grupo_de_acesso"
  }
  return (
    <IndexView data={show} module='permissoes' itemMenu='grupodeacesso'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
