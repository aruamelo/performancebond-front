import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import grupoDeAcessoService from "../../../../service/permissoes/GrupoDeAcessoService";

export default function Index () {
  const [grupoDeAcesso, setGrupoDeAcesso] = React.useState(null);

  React.useEffect(() => {
    grupoDeAcessoService.gruposdeacesso().then(data => {
      setGrupoDeAcesso(data.data)
    })
  }, [])

  if (!grupoDeAcesso) return null;

  const columns = [
    { field: 'name', header: 'Nome' }
  ]

  return (
    <IndexDatatable columns={columns} data={grupoDeAcesso} urlDelete='grupodeacesso/delete'/>
  )
}
