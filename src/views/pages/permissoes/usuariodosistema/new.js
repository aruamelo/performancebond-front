import React from 'react'
import { IconChevronRight } from '@tabler/icons-react'
import Container from 'react-bootstrap/Container';
import '../../../../assets/performancebond/css/perfomancebond.css'
import BreadcrumbsSubPage from "../../../../ui-component/extended/BreadcrumbsSubPage";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { useFormik } from 'formik';
import { classNames } from 'primereact/utils';
import request from "../../../../request/request";
import noty from "../../../../alert/noty";
import distributedFunctions from "../../../../request/distributedFunctions";
import {ButtonDefaultForm} from "../../../../ui-component/button/ButtonDefaultForm";
import Select2MultipleAsync from "../../../../ui-component/select2/select2Multiple";
import LoadingPerformancebond from "../../../../ui-component/loading/LoadingPerformancebond";

export default function Index () {
    const [loading, setLoading] = React.useState(false);

    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            role_id: []
        },
        validate: (data) => {
            let errors = {};

            if (!data.name) {
                errors.name = 'Nome é obrigatório.';
            }

            if (!data.role_id) {
                errors.role_id = 'Grupo de acesso é obrigatório.';
            }

            if (!data.email) {
                errors.email = 'Email é obrigatório.';
            }

            if (!data.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(data.email)) {
                errors.email = 'Email inválido. Ex.: example@email.com';
            }

            return errors;
        },
        onSubmit: (data) => {
            setLoading(true)
            data.contrato_id = distributedFunctions.getIdContratoUser()
            request.post('usuariodosistema/usuarioporcontrato', data).then(data => {
                noty.success(data.data.message)
                distributedFunctions.redirectPage('index')
            }).catch((err) => {
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const breadcrumb =  {
            "id": "usuariodosistema",
            "title": "Usuário do sistema",
            "titlepage": "Cadastrar usuário",
            "permission": "usuario_do_sistema"
    }

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />
            <form onSubmit={formik.handleSubmit}>
                <div className="card">
                    <Row key={'rowForm'}>
                        <Col xs={6} md={4} key={'colName'}>
                            <Form.Label htmlFor="name">Nome</Form.Label>
                            <Form.Control
                                type="text"
                                id="name"
                                name="name"
                                value={formik.values.name}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o nome do novo usuário"
                                onChange={formik.handleChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('name') })}
                            />
                            {getFormErrorMessage('name')}
                        </Col>
                        <Col xs={6} md={4} key={'colEmail'}>
                            <Form.Label htmlFor="email">Email</Form.Label>
                            <Form.Control
                                type="text"
                                id="email"
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o email do novo usuário"
                                onChange={formik.handleChange}
                                name="email"
                                value={formik.values.email}
                                className={classNames({ 'p-invalid': isFormFieldValid('email') })}
                            />
                            {getFormErrorMessage('email')}
                        </Col>
                        <Col xs={6} md={4} key={'colGrupoDeAcesso'}>
                            <Form.Label htmlFor="inputNome">Grupo de acesso</Form.Label>
                            <Select2MultipleAsync url="grupodeacesso/grupodeacessoselect2"
                                          placeholder="Selecionar um grupo de acesso"
                                          name="role_id"
                                          className={classNames({ 'p-invalid': isFormFieldValid('role_id') })}
                                          formik ={formik}/>
                            {getFormErrorMessage('role_id')}
                        </Col>
                    </Row>
                </div>

                <ButtonDefaultForm/>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>

    )
}
