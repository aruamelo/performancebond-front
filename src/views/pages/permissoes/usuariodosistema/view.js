import React from 'react'
import { useParams } from "react-router-dom";
import usuarioDoSistemaService from '../../../../service/permissoes/usuarioDoSistemaService'
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [usuario, setUsuario] = React.useState(null);

  React.useEffect(() => {
    usuarioDoSistemaService.showUsuario(id).then(data => {
      setUsuario(data.data)
    })
  }, [])

  if (!usuario) return null;

  const show = [
    { field: 'name', label: 'Nome', value: usuario.name },
    { field: 'email', label: 'E-mail', value: usuario.email },
    { field: 'created_at', label: 'Data de criação', value: usuario.created_at }
  ]
  const breadcrumb =  {
    "id": "usuariodosistema",
    "title": "Usuário do sistema",
    "titlepage": "Informações do usuário",
    "permission": "parametrizacao_do_sistema__codificacao_de_documento"
  }
  return (
    <IndexView data={show} module='permissoes' itemMenu='usuariodosistema'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
