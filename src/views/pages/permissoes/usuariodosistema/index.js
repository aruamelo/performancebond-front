import React from 'react'
import usuarioDoSistemaService from '../../../../service/permissoes/usuarioDoSistemaService'
import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { Button } from 'primereact/button'
import distributedFunctions from "../../../../request/distributedFunctions";
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import Stack from 'react-bootstrap/Stack'
import request from "../../../../request/request";
import noty from "../../../../alert/noty";
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';

export default function Index () {
  const [usuario, setUsuario] = React.useState(null);

  const deleteClick = (id) => {
    confirmDialog({
      message: 'Deseja deletar o registro?',
      header: 'Deletar o registro',
      icon: 'pi pi-info-circle',
      acceptLabel: 'Sim',
      rejectLabel: 'Não',
      accept: () => acceptFunc(id)
    });
  }

  const resetarSenha = (id) => {
    confirmDialog({
        message: 'Deseja resetar a senha do usuário?',
        header: 'Resetar senha',
        icon: 'pi pi-info-circle',
        acceptLabel: 'Sim',
        rejectLabel: 'Não',
        accept: () => resetarSenhaFunc(id)
    });
  }
  const acceptFunc = (id) => {
    request.delete(id, 'usuariodosistema/usuarioporcontrato').then(data => {
      noty.success(data.data.message)

      // Filtrar as informações de todas as linhas para remover o item selecionado
      let row = usuario.filter(item => {
        return item.id !== id
      })

      setUsuario(row);
    }).catch((err) => {
      let responseErro = JSON.parse(err.request.response)
      noty.error(responseErro.message)
    })
  }

  const resetarSenhaFunc = (id) => {
    request.post('usuariodosistema/alterarsenha', {user_id:id}).then(data => {
        noty.success(data.data.message)
    }).catch((err) => {
        let responseErro = JSON.parse(err.request.response)
        noty.error(responseErro.message)
    })
  }

  React.useEffect(() => {
    const idContrato = distributedFunctions.getIdContratoUser()
    usuarioDoSistemaService.usuariosPorContrato(idContrato).then(data => {
      let dadosContrato = data.data

      dadosContrato.map((register) => {
            register.acaoBotao =
                <Stack direction="horizontal" gap={3}>
                  <Button icon="pi pi-eye"
                          tooltip="Visualizar"
                          tooltipOptions={{ position: 'bottom' }}
                          rounded className="p-2"
                          severity="secondary"
                          onClick={() => distributedFunctions.redirectPage('view', register.id)}/>

                  <Button icon="pi pi-file-edit"
                          tooltip="Editar"
                          tooltipOptions={{ position: 'bottom' }}
                          rounded className="p-2"
                          onClick={() => distributedFunctions.redirectPage( 'edit', register.id)}/>

                  <Button icon="pi pi-trash"
                          tooltip="Deletar"
                          tooltipOptions={{ position: 'bottom' }}
                          rounded
                          severity="danger"
                          onClick={() => deleteClick(register.id)}/>

                    <Button icon="pi pi-key"
                            tooltip="Resetar a senha"
                            tooltipOptions={{ position: 'bottom' }}
                            rounded
                            severity="warning"
                            onClick={() => resetarSenha(register.id)}/>
                </Stack>
          }
      )
      setUsuario(dadosContrato);
    })
  }, [])

  if (!usuario) return null;

  return (
      <Container fluid>
        <Row>
          <Col>
            <Button
                className="p-button-raised p-button-rounded"
                tooltip="Novo"
                tooltipOptions={{ position: 'bottom' }}
                icon="pi pi-plus"
                onClick={() => distributedFunctions.redirectPage('new')}/>
          </Col>
        </Row>
        <br/><br/>
        <Row>
          <div className="card">
            <DataTable
                value={usuario}
                paginator
                rows={25}
                first={0}
                onPage={0}
                size="large"
                sortable = "true"
                responsiveLayout="scroll"
                responsive = "true"
                dataKey="id"
                filterDisplay="row"
                emptyMessage="Registro não encontrado."
            >
              <Column key={'name'} field={'name'} header={'Nome'} filter sortable />
              <Column key={'email'} field={'email'} header={'E-mail'} filter sortable />
              <Column key="acaoBotao" field="acaoBotao" header="Ação" />
            </DataTable>
            <ConfirmDialog />
          </div>
        </Row>
      </Container>
    // <IndexDatatable columns={columns} data={usuario} urlDelete='usuariodosistema/usuarioporcontrato'/>
  )
}
