import React from 'react'
import { DatatableCustom } from '../../../ui-component/datatable/DatatableCustom'
import usuarioDoSistemaService from '../../../service/permissoes/usuarioDoSistemaService'
import distributedFunctions from "../../../request/distributedFunctions";

export default function UsuarioDoSistema () {
  const [usuario, setUsuario] = React.useState(null);

  React.useEffect(() => {
    const idContrato = distributedFunctions.getIdContratoUser()
    usuarioDoSistemaService.usuariosPorContrato(idContrato).then(data => {
      setUsuario(data.data)
    })
  }, [])

  if (!usuario) return null;

  const columns = [
    { field: 'name', header: 'Nome' },
    { field: 'email', header: 'E-mail' }
  ]

  return (
    <DatatableCustom columns={columns} row={usuario} urlDelete='usuariodosistema/usuarioporcontrato'/>
  )
}
