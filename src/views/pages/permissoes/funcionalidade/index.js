import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import funcionalidadeService from "../../../../service/permissoes/FuncionalidadeService";

export default function Index () {
  const [funcionalidade, setFuncionalidade] = React.useState(null);

  React.useEffect(() => {
    funcionalidadeService.funcionalidades().then(data => {
      setFuncionalidade(data.data)
    })
  }, [])

  if (!funcionalidade) return null;

  const columns = [
    { field: 'name', header: 'Nome' }
  ]

  return (
    <IndexDatatable columns={columns} data={funcionalidade} urlDelete='funcionalidade/delete'/>
  )
}
