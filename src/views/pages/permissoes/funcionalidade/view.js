import React from 'react'
import { useParams } from "react-router-dom";
import funcionalidadeService from "../../../../service/permissoes/FuncionalidadeService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [funcionalidade, setFuncionalidade] = React.useState(null);

  React.useEffect(() => {
    funcionalidadeService.showFuncionalidade(id).then(data => {
      setFuncionalidade(data.data)
    })
  }, [])

  if (!funcionalidade) return null;

  const show = [
    { field: 'name', label: 'Nome', value: funcionalidade.name },
    { field: 'created_at', label: 'Data de criação', value: funcionalidade.created_at }
  ]
  const breadcrumb =  {
    "id": "funcionalidade",
    "title": "Funcionalidade do sistema",
    "titlepage": "Informações da funcionalidade",
    "permission": "funcionalidades"
  }
  return (
    <IndexView data={show} module='permissoes' itemMenu='funcionalidade'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
