import React from 'react'
import { useParams } from "react-router-dom";
import disciplinaService from "../../../../service/codificacaodedocumento/nucleos/DisciplinaService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [disciplina, setDisciplina] = React.useState(null);

  React.useEffect(() => {
    disciplinaService.showDisciplina(id).then(data => {
      setDisciplina(data.data)
    })
  }, [])

  if (!disciplina) return null;

  const show = [
    { field: 'codigo', label: 'Código', value: disciplina.codigo },
    { field: 'descricao', label: 'Descrição', value: disciplina.descricao },
    { field: 'created_at', label: 'Data de criação', value: disciplina.created_at }
  ]
  const breadcrumb =  {
    "id": "disciplinas",
    "title": "Disciplinas",
    "titlepage": "Informações da disciplina",
    "permission": "codificacao_de_documento__disciplinas"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='disciplinas'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
