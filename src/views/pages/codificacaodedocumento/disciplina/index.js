import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import disciplinaService from "../../../../service/codificacaodedocumento/nucleos/DisciplinaService";

export default function Index () {
  const [disciplina, setDisciplina] = React.useState(null);

  React.useEffect(() => {
    disciplinaService.disciplinas().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setDisciplina(data.data)
    })
  }, [])

  if (!disciplina) return null;

  const columns = [
    { field: 'codigo', header: 'Código' },
    { field: 'descricao', header: 'Descrição' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={disciplina} urlDelete='codificacaodedocumento/disciplinas/disciplinas'/>
  )
}
