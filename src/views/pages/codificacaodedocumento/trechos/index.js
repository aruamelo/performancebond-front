import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import trechosService from "../../../../service/codificacaodedocumento/trechos/TrechosService";

export default function Index () {
  const [trecho, setTrecho] = React.useState(null);

  React.useEffect(() => {
    trechosService.trechos().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setTrecho(data.data)
    })
  }, [])

  if (!trecho) return null;

  const columns = [
    { field: 'codigo', header: 'Código' },
    { field: 'descricao', header: 'Descrição' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={trecho} urlDelete='codificacaodedocumento/trechos/trechos'/>
  )
}
