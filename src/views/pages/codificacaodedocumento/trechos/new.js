import React from 'react'
import { IconChevronRight } from '@tabler/icons-react'
import Container from 'react-bootstrap/Container';
import '../../../../assets/performancebond/css/perfomancebond.css'
import BreadcrumbsSubPage from "../../../../ui-component/extended/BreadcrumbsSubPage";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useFormik } from 'formik';
import { classNames } from 'primereact/utils';
import noty from "../../../../alert/noty";
import distributedFunctions from "../../../../request/distributedFunctions";
import {ButtonDefaultForm} from "../../../../ui-component/button/ButtonDefaultForm";
import trechosService from "../../../../service/codificacaodedocumento/trechos/TrechosService";
import frenteDeServicoService from "../../../../service/codificacaodedocumento/frentedeservico/FrenteDeServicoService";
import { Checkbox } from 'primereact/checkbox';
import LoadingPerformancebond from "../../../../ui-component/loading/LoadingPerformancebond";

export default function Index () {
    const [loading, setLoading] = React.useState(false);

    const formik = useFormik({
        initialValues: {
            codigo: '',
            descricao: '',
            contrato_id: distributedFunctions.getIdContratoUser(),
            ativo: true,
            frente_de_servicos: []
        },
        validate: (data) => {
            let errors = {};

            if (!data.descricao) {
                errors.descricao = 'Descrição do trecho é obrigatório.';
            }

            if (!data.codigo) {
                errors.codigo = 'Código do trecho é obrigatório.';
            }

            if (data.codigo.length > 5) {
                errors.codigo = 'Código do trecho deve ser menor que cinco caracteres.';
            }

            if (data.descricao.length > 255) {
                errors.codigo = 'Descrição do trecho deve ser menor que duzentos e cinquenta e cinco caracteres.';
            }

            if (cities.length === 0) {
                errors.frente_de_servicos = 'O trecho deve ter ao menos uma frente de serviço para realizar o cadastro';
            }

            return errors;
        },
        onSubmit: (data) => {
            setLoading(true)
            data.frente_de_servicos = cities
            trechosService.insertTrecho(data).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    distributedFunctions.redirectPage('index')
                }, 2000)
            }).catch((err) => {
                if (err.response.status === 401) {
                    window.location.href = '/pb'
                }
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const breadcrumb =  {
            "id": "trechos",
            "title": "Trechos",
            "titlepage": "Cadastrar trecho",
            "permission": "frente_de_servico__codificacao_de_documento__frente_de_servico"
    }

    const [dadosCarregados, setDadosCarregados] = React.useState(false);
    const [frenteDeServico, setFrenteDeServico] = React.useState(null);
    const [cities, setCities] = React.useState([]);

    React.useEffect(() => {
        frenteDeServicoService.frenteDeServicos().then((data) => {
            setFrenteDeServico(data.data)
            setDadosCarregados(true);
        });
    }, []);

    // Renderizar apenas quando os dados estiverem totalmente carregados
    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    const onCityChange = (e) => {
        let selectedCities = [...cities];
        if(e.checked)
            selectedCities.push(e.value);
        else
            selectedCities.splice(selectedCities.indexOf(e.value), 1);

        setCities(selectedCities);
    }

    const checkBoxGrupoDeAcesso =  frenteDeServico.map((frente) => {
        return <div className="col-md-4 mt-2" key={frente.id}>
            <Checkbox inputId={frente.id} value={frente.id} onChange={onCityChange}
                      checked={cities.includes(frente.id)} name="frente_de_servicos"></Checkbox>
            <label htmlFor={frente.id} className={classNames({ 'p-invalid': isFormFieldValid('frente_de_servicos') })}> {frente.codigo} - {frente.descricao} </label>
        </div>
    })

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />
            <form onSubmit={formik.handleSubmit}>
                <div className="card">
                    <Row key={'rowForm'}>
                        <Col xs={6} md={6} key={'colCodigo'}>
                            <Form.Label htmlFor="codigo">Código</Form.Label>
                            <Form.Control
                                type="text"
                                id="codigo"
                                name="codigo"
                                value={formik.values.codigo}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o código da frente de serviço"
                                onChange={formik.handleChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('codigo') })}
                            />
                            {getFormErrorMessage('codigo')}
                        </Col>
                        <Col xs={6} md={6} key={'colName'}>
                            <Form.Label htmlFor="name">Nome</Form.Label>
                            <Form.Control
                                type="text"
                                id="descricao"
                                name="descricao"
                                value={formik.values.descricao}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o nome da frente de serviço"
                                onChange={formik.handleChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('descricao') })}
                            />
                            {getFormErrorMessage('descricao')}
                        </Col>
                    </Row>
                    <Row key={'rowFrenteDeServico'}>
                        <Form.Label htmlFor="name" className="mt-4">Frente de serviço para associar ao trecho</Form.Label>
                        {getFormErrorMessage('frente_de_servicos')}
                        {checkBoxGrupoDeAcesso}
                    </Row>
                </div>

                <ButtonDefaultForm/>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>

    )
}
