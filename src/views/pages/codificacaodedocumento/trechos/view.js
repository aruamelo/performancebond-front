import React from 'react'
import { useParams } from "react-router-dom";
import trechosService from "../../../../service/codificacaodedocumento/trechos/TrechosService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [trecho, setTrecho] = React.useState(null);

  React.useEffect(() => {
    trechosService.showTrecho(id).then(data => {
      setTrecho(data.data)
    })
  }, [])

  if (!trecho) return null;

  const show = [
    { field: 'codigo', label: 'Código', value: trecho.codigo },
    { field: 'descricao', label: 'Descrição', value: trecho.descricao },
    { field: 'created_at', label: 'Data de criação', value: trecho.created_at }
  ]
  const breadcrumb =  {
    "id": "trecho",
    "title": "Trecho",
    "titlepage": "Informações do trecho",
    "permission": "codificacao_de_documento__trechos"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='trechos'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
