import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import faseDoProjetoService from "../../../../service/codificacaodedocumento/fasedoprojeto/FaseDoProjetoService";

export default function Index () {
  const [faseDoProjeto, setFaseDoProjeto] = React.useState(null);

  React.useEffect(() => {
    faseDoProjetoService.fasesDoProjetoService().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setFaseDoProjeto(data.data)
    })
  }, [])

  if (!faseDoProjeto) return null;

  const columns = [
    { field: 'codigo', header: 'Código' },
    { field: 'descricao', header: 'Descrição' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={faseDoProjeto} urlDelete='codificacaodedocumento/fasedoprojeto/fasesdoprojeto'/>
  )
}
