import React from 'react'
import { useParams } from "react-router-dom";
import faseDoProjetoService from "../../../../service/codificacaodedocumento/fasedoprojeto/FaseDoProjetoService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [faseDoProjeto, setFaseDoProjeto] = React.useState(null);

  React.useEffect(() => {
    faseDoProjetoService.showFaseDoProjeto(id).then(data => {
      setFaseDoProjeto(data.data)
    })
  }, [])

  if (!faseDoProjeto) return null;

  const show = [
    { field: 'codigo', label: 'Código', value: faseDoProjeto.codigo },
    { field: 'descricao', label: 'Descrição', value: faseDoProjeto.descricao },
    { field: 'created_at', label: 'Data de criação', value: faseDoProjeto.created_at }
  ]
  const breadcrumb =  {
    "id": "fasedoprojeto",
    "title": "Fase do projeto",
    "titlepage": "Informações da fase do projeto",
    "permission": "codificacao_de_documento__fases_do_projeto"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='fasedoprojeto'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
