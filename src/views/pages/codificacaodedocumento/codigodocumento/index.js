import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import codigoDocumentoService from "../../../../service/codificacaodedocumento/codigodocumento/CodigoDocumentoService";

export default function Index () {
  const [codigoDocumento, setCodigoDocumento] = React.useState(null);

  React.useEffect(() => {
    codigoDocumentoService.codigosDocumento().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setCodigoDocumento(data.data)
    })
  }, [])

  if (!codigoDocumento) return null;

  const columns = [
    { field: 'codigo', header: 'Código' },
    { field: 'descricao', header: 'Descrição' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={codigoDocumento} urlDelete='codificacaodedocumento/codigodocumento/codigosdocumento'/>
  )
}
