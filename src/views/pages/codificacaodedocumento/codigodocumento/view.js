import React from 'react'
import { useParams } from "react-router-dom";
import codigoDocumentoService from "../../../../service/codificacaodedocumento/codigodocumento/CodigoDocumentoService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [codigoDocumento, setCodigoDocumento] = React.useState(null);

  React.useEffect(() => {
    codigoDocumentoService.showcodigoDocumento(id).then(data => {
      setCodigoDocumento(data.data)
    })
  }, [])

  if (!codigoDocumento) return null;

  const show = [
    { field: 'codigo', label: 'Código', value: codigoDocumento.codigo },
    { field: 'descricao', label: 'Descrição', value: codigoDocumento.descricao },
    { field: 'created_at', label: 'Data de criação', value: codigoDocumento.created_at }
  ]
  const breadcrumb =  {
    "id": "codigodocumento",
    "title": "Código do documento",
    "titlepage": "Informações do código do documento",
    "permission": "codificacao_de_documento__codigo_do_documento"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='codigodocumento'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
