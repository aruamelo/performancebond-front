import React from 'react'
import { IconChevronRight } from '@tabler/icons-react'
import Container from 'react-bootstrap/Container';
import '../../../../assets/performancebond/css/perfomancebond.css'
import BreadcrumbsSubPage from "../../../../ui-component/extended/BreadcrumbsSubPage";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { useFormik } from 'formik';
import { classNames } from 'primereact/utils';
import codigoDocumentoService from "../../../../service/codificacaodedocumento/codigodocumento/CodigoDocumentoService";
import noty from "../../../../alert/noty";
import distributedFunctions from "../../../../request/distributedFunctions";
import {ButtonDefaultForm} from "../../../../ui-component/button/ButtonDefaultForm";
import LoadingPerformancebond from "../../../../ui-component/loading/LoadingPerformancebond";

export default function Index () {
    const [loading, setLoading] = React.useState(false);

    const formik = useFormik({
        initialValues: {
            codigo: '',
            descricao: '',
            contrato_id: distributedFunctions.getIdContratoUser(),
            ativo: true
        },
        validate: (data) => {
            let errors = {};

            if (!data.descricao) {
                errors.descricao = 'Descrição do código do documento é obrigatório.';
            }

            if (!data.codigo) {
                errors.codigo = 'Código do código do documento é obrigatório.';
            }

            if (data.codigo.length > 6) {
                errors.codigo = 'Código do código do documento deve ser menor que seis caracteres.';
            }

            if (data.descricao.length > 255) {
                errors.codigo = 'Descrição do código do documento deve ser menor que duzentos e cinquenta e cinco caracteres.';
            }

            return errors;
        },
        onSubmit: (data) => {
            setLoading(true)
            codigoDocumentoService.insertcodigoDocumento(data).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    distributedFunctions.redirectPage('index')
                }, 2000)
            }).catch((err) => {
                if (err.response.status === 401) {
                    window.location.href = '/pb'
                }
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const breadcrumb =  {
            "id": "codigodocumento",
            "title": "Código do documento",
            "titlepage": "Cadastrar código do documento",
            "permission": "codificacao_de_documento__codigo_do_documento"
    }

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />
            <form onSubmit={formik.handleSubmit}>
                <div className="card">
                    <Row key={'rowForm'}>
                        <Col xs={6} md={6} key={'codigo'}>
                            <Form.Label htmlFor="codigo">Código</Form.Label>
                            <Form.Control
                                type="text"
                                id="codigo"
                                name="codigo"
                                value={formik.values.codigo}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o código do código do documento"
                                onChange={formik.handleChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('codigo') })}
                            />
                            {getFormErrorMessage('codigo')}
                        </Col>
                        <Col xs={6} md={6} key={'nome'}>
                            <Form.Label htmlFor="name">Nome</Form.Label>
                            <Form.Control
                                type="text"
                                id="descricao"
                                name="descricao"
                                value={formik.values.descricao}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o nome do código do documento"
                                onChange={formik.handleChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('descricao') })}
                            />
                            {getFormErrorMessage('descricao')}
                        </Col>
                    </Row>
                </div>

                <ButtonDefaultForm/>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>

    )
}
