import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import subcontratosService from "../../../../service/codificacaodedocumento/subcontratos/SubcontratosService";

export default function Index () {
  const [subcontrato, setSubcontrato] = React.useState(null);

  React.useEffect(() => {
    subcontratosService.subcontratos().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setSubcontrato(data.data)
    })
  }, [])

  if (!subcontrato) return null;

  const columns = [
    { field: 'codigo', header: 'Código' },
    { field: 'descricao', header: 'Descrição' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={subcontrato}
                    urlDelete='codificacaodedocumento/subcontratos/subcontratos'/>
  )
}
