import React from 'react'
import { useParams } from "react-router-dom";
import subcontratosService from "../../../../service/codificacaodedocumento/subcontratos/SubcontratosService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [subcontrato, setSubcontrato] = React.useState(null);

  React.useEffect(() => {
    subcontratosService.showSubcontrato(id).then(data => {
      setSubcontrato(data.data)
    })
  }, [])

  if (!subcontrato) return null;

  const show = [
    { field: 'codigo', label: 'Código', value: subcontrato.codigo },
    { field: 'descricao', label: 'Descrição', value: subcontrato.descricao },
    { field: 'created_at', label: 'Data de criação', value: subcontrato.created_at }
  ]
  const breadcrumb =  {
    "id": "subcontratos",
    "title": "Subcontratos",
    "titlepage": "Informações do subcontrato",
    "permission": "codificacao_de_documento__subcontratos"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='subcontratos'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
