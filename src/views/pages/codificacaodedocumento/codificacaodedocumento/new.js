import React from 'react'
import { IconChevronRight } from '@tabler/icons-react'
import Container from 'react-bootstrap/Container';
import '../../../../assets/performancebond/css/perfomancebond.css'
import BreadcrumbsSubPage from "../../../../ui-component/extended/BreadcrumbsSubPage";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import { useFormik } from 'formik';
import codificacaoDeDocumentoService from "../../../../service/codificacaodedocumento/codificacaodedocumento/CodificacaoDeDocumentoService";
import noty from "../../../../alert/noty";
import distributedFunctions from "../../../../request/distributedFunctions";
import {ButtonDefaultForm} from "../../../../ui-component/button/ButtonDefaultForm";
import { InputNumber } from 'primereact/inputnumber';
import { classNames } from 'primereact/utils';
import Col from 'react-bootstrap/Col';
import LoadingPerformancebond from "../../../../ui-component/loading/LoadingPerformancebond";

export default function Index () {
    const [loading, setLoading] = React.useState(false);
    const [dadosCarregados, setDadosCarregados] = React.useState(false);
    const [campoParametrizacao, setCampoParametrizacao] = React.useState(null);
    // const [cities, setCities] = React.useState([]);

    const formik = useFormik({
        initialValues: {
            posicao: [],
            codigo: [],
            hint: [],
            contrato_id: distributedFunctions.getIdContratoUser()
        },
        onSubmit: (data) => {
            setLoading(true)
            codificacaoDeDocumentoService.insertCodificacaoDeDocumento(data).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    distributedFunctions.redirectPage('index')
                }, 2000)
            }).catch((err) => {
                if (err.response.status === 401) {
                    window.location.href = '/pb'
                }
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const breadcrumb =  {
            "id": "parametrizacaocodificacaodedocumento",
            "title": "Parametrizar codificação de documento",
            "titlepage": "Parametrizar codificação de documento",
            "permission": "codificacao_de_documento__parametrizar_codificacao_de_documento"
    }

    React.useEffect(() => {
        codificacaoDeDocumentoService.getCamposParametrizarCodificacaoDeDocumento().then((data) => {
            setCampoParametrizacao(data.data)
            setDadosCarregados(true);
        });
    }, []);

    // Renderizar apenas quando os dados estiverem totalmente carregados
    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    const onPosicaoChange = (e, posicao) => {
        let selectedCities1 = { ...formik.values.posicao };
        const newValue = e.value !== null ? Math.max(1, Math.min(e.value, 8)) : null;

        // Verifica se o novo valor já está em uso
        const isValueUsed = Object.values(selectedCities1).includes(newValue);

        if (isValueUsed) {
            return;
        }

        // Remover os valores apagados pelo usuário
        if (newValue === null) {
            const newSelectedCities = Object.assign({}, selectedCities1);
            delete newSelectedCities[posicao.id];
            formik.setFieldValue('posicao', newSelectedCities);
            return;
        }

        selectedCities1[posicao.id] = newValue;
        formik.setFieldValue('posicao', selectedCities1);
    };

    const onCodigoChange = (e, campo) => {
        const newValues = [...formik.values.codigo]; // Crie uma cópia do array

        // Atualize o valor do campo no array
        newValues[campo.id] = e.currentTarget.value;

        // Atualize o estado do formulário
        formik.setFieldValue('codigo', newValues);
    };

    const onHintChange = (e, campo) => {
        const newValues = [...formik.values.hint]; // Crie uma cópia do array

        // Atualize o valor do campo no array
        newValues[campo.id] = e.currentTarget.value;

        // Atualize o estado do formulário
        formik.setFieldValue('hint', newValues);
    };


    const inputTextCampos = campoParametrizacao.map((campo) => {
        return (
            <Row key={`rowFrenteDeServico-${campo.id}`} className="mt-4">
                <label htmlFor={campo.id} style={{display: 'block'}}>
                    {campo.texto_exibicao}
                </label>

                <Col xs={2} md={1} key={`colPosicao-${campo.id}`} className="mt-4">
                    {getFormErrorMessage('posicao')}
                    <label htmlFor={campo.id} style={{display: 'block'}}>
                        Posição
                    </label>
                    <InputNumber
                        inputId={campo.id}
                        onValueChange={(e) => onPosicaoChange(e, campo)}
                        mode="decimal"
                        showButtons
                        buttonLayout="vertical"
                        style={{width: '4rem'}}
                        decrementButtonClassName="p-button-secondary"
                        incrementButtonClassName="p-button-secondary"
                        incrementButtonIcon="pi pi-plus"
                        decrementButtonIcon="pi pi-minus"
                        min={1}
                        max={8}
                        // name={`campos[${campo.id}]`}
                        className={classNames({'p-invalid': isFormFieldValid('posicao')})}
                    />
                </Col>

                <Col xs={6} md={1} key={`colCodigo${campo.id}`} className="mt-4">
                    <Form.Label htmlFor="codigo">Código</Form.Label>
                    <Form.Control
                        type="text"
                        id={`codigo-${campo.id}`} // Adicione um ID único para cada campo
                        value={formik.values.codigo[campo.id] || ""} // Acesse o valor correto dentro do array
                        aria-describedby="passwordHelpBlock"
                        onChange={(e) => onCodigoChange(e, campo)}
                        className={classNames({'p-invalid': isFormFieldValid('codigo')})}
                    />
                    {getFormErrorMessage('codigo')}
                </Col>

                <Col xs={6} md={10} key={`colHint${campo.id}`} className="mt-4">
                    <Form.Label htmlFor="hint">Hint de informativo</Form.Label>
                    <Form.Control
                        type="text"
                        id={`hint-${campo.id}`} // Adicione um ID único para cada campo
                        value={formik.values.hint[campo.id] || ""} // Acesse o valor correto dentro do array
                        aria-describedby="passwordHelpBlock"
                        placeholder="Informar o hint de exibição para o campo(opcional)"
                        onChange={(e) => onHintChange(e, campo)}
                        className={classNames({'p-invalid': isFormFieldValid('hint')})}
                    />
                    {getFormErrorMessage('hint')}
                </Col>
            </Row>
        );
    });

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />
            <form onSubmit={formik.handleSubmit}>
                <div className="card">
                    <Row key={'rowLabelFixo'}>
                        <Form.Label htmlFor="name" className="mt-4">
                            Informar a ordem em que cada campo ficará na EAP
                        </Form.Label>
                        {inputTextCampos}
                    </Row>
                </div>

                <ButtonDefaultForm />
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>
    );
}
