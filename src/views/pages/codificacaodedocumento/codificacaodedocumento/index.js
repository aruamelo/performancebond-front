import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import codificacaoDeDocumentoService from "../../../../service/codificacaodedocumento/codificacaodedocumento/CodificacaoDeDocumentoService";

export default function Index () {
  const [subcontrato, setSubcontrato] = React.useState(null);

  React.useEffect(() => {
    codificacaoDeDocumentoService.codificacoesDeDocumento().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setSubcontrato(data.data)
    })
  }, [])

  if (!subcontrato) return null;

  const columns = [
    { field: 'eap_exibicao', header: 'EAP' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={subcontrato}
                    urlDelete='codificacaodedocumento/codificacaodedocumento/codificacaodedocumento'/>
  )
}
