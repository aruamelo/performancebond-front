import React from 'react'
import { useParams } from "react-router-dom";
import codificacaoDeDocumentoService from "../../../../service/codificacaodedocumento/codificacaodedocumento/CodificacaoDeDocumentoService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [codificacaoDeDocumento, setCodificacaoDeDocumento] = React.useState(null);

  React.useEffect(() => {
    codificacaoDeDocumentoService.showCodificacaoDeDocumento(id).then(data => {
      setCodificacaoDeDocumento(data.data)
    })
  }, [])

  if (!codificacaoDeDocumento) return null;

  const show = [
    { field: 'eap_exibicao', label: 'Eap exibição', value: codificacaoDeDocumento.eap_exibicao },
    { field: 'created_at', label: 'Data de criação', value: codificacaoDeDocumento.created_at }
  ]
  const breadcrumb =  {
    "id": "subcontratos",
    "title": "Subcontratos",
    "titlepage": "Informações do subcontrato",
    "permission": "codificacao_de_documento__subcontratos"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='subcontratos'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
