import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import frenteDeServicoService from "../../../../service/codificacaodedocumento/frentedeservico/FrenteDeServicoService";

export default function Index () {
  const [frenteDeServico, setFrenteDeServico] = React.useState(null);

  React.useEffect(() => {
    frenteDeServicoService.frenteDeServicos().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setFrenteDeServico(data.data)
    })
  }, [])

  if (!frenteDeServico) return null;

  const columns = [
    { field: 'codigo', header: 'Código' },
    { field: 'descricao', header: 'Descrição' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={frenteDeServico} urlDelete='codificacaodedocumento/frentedeservico/frentedeservicos'/>
  )
}
