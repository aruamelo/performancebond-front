import React from 'react'
import { IconChevronRight } from '@tabler/icons-react'
import Container from 'react-bootstrap/Container';
import '../../../../assets/performancebond/css/perfomancebond.css'
import BreadcrumbsSubPage from "../../../../ui-component/extended/BreadcrumbsSubPage";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { useFormik } from 'formik';
import { classNames } from 'primereact/utils';
import noty from "../../../../alert/noty";
import distributedFunctions from "../../../../request/distributedFunctions";
import {ButtonDefaultForm} from "../../../../ui-component/button/ButtonDefaultForm";
import frenteDeServicoService from "../../../../service/codificacaodedocumento/frentedeservico/FrenteDeServicoService";
import {useParams } from 'react-router-dom';
import { Checkbox } from 'primereact/checkbox';
import LoadingPerformancebond from "../../../../ui-component/loading/LoadingPerformancebond";

export default function Edit () {
    const { id } = useParams();
    const [loading, setLoading] = React.useState(false);

    const formik = useFormik({
        initialValues: {
            codigo: '',
            descricao: '',
            ativo: true
        },
        validate: (data) => {
            let errors = {};

            if (!data.descricao) {
                errors.descricao = 'Descrição da frente de serviço é obrigatório.';
            }

            if (!data.codigo) {
                errors.codigo = 'Código da frente de serviço é obrigatório.';
            }

            if (data.codigo.length > 5) {
                errors.codigo = 'Código da frente de serviço deve ser menor que cinco caracteres.';
            }

            if (data.descricao.length > 255) {
                errors.descricao = 'Descrição da frente de serviço deve ser menor que duzentos e cinquenta e cinco caracteres.';
            }

            return errors;
        },
        onSubmit: (data) => {
            setLoading(true)
            frenteDeServicoService.editFrenteDeServico(data).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    distributedFunctions.redirectPage('../index')
                }, 2000)
            }).catch((err) => {
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const handleInputChange = (e) => {
        const { name, value, checked } = e.target;
        formik.setFieldValue(name, name === 'ativo' ? checked : value);
    };

    const [dadosCarregados, setDadosCarregados] = React.useState(false);

    React.useEffect(() => {
        frenteDeServicoService.showFrenteDeServico(id).then((data) => {
            // Configurar o estado inicial do formik com os dados do usuário
            formik.setValues({
                id: data.data.id,
                codigo: data.data.codigo,
                descricao: data.data.descricao,
                ativo: data.data.ativo,
                contrato_id: distributedFunctions.getIdContratoUser()
            });
            setDadosCarregados(true);
        });
    }, [id]);

    // Renderizar apenas quando os dados estiverem totalmente carregados
    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    const breadcrumb =  {
        "id": "frentedeservico",
        "title": "Frente de serviço",
        "titlepage": "Editar frente de serviço",
        "permission": "frente_de_servico__codificacao_de_documento__frente_de_servico"
    }

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />
            <form onSubmit={formik.handleSubmit}>
                <div className="card">
                    <Row key={'rowForm'}>
                        <Col xs={6} md={6} key={'colCodigo'}>
                            <Form.Label htmlFor="codigo">Código</Form.Label>
                            <Form.Control
                                type="text"
                                id="codigo"
                                name="codigo"
                                value={formik.values.codigo}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o código da frente de serviço"
                                onChange={handleInputChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('codigo') })}
                            />
                            {getFormErrorMessage('codigo')}
                        </Col>
                        <Col xs={6} md={6} key={'colName'}>
                            <Form.Label htmlFor="name">Nome</Form.Label>
                            <Form.Control
                                type="text"
                                id="descricao"
                                name="descricao"
                                value={formik.values.descricao}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o nome da frente de serviço"
                                onChange={handleInputChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('descricao') })}
                            />
                            {getFormErrorMessage('descricao')}
                        </Col>
                    </Row>
                    <Row key={'rowAtivo'}>
                        <Col xs={6} md={5} key={'colAtivo'}>
                            <Checkbox inputId="ativo" value={formik.values.ativo}
                                      checked={formik.values.ativo} name="ativo"
                                      onChange={handleInputChange} className="mt-4"></Checkbox>
                            <label htmlFor="ativo"
                                   className={classNames({ 'p-invalid': isFormFieldValid('ativo') })}>
                                Ativo </label>
                        </Col>
                    </Row>
                </div>

                <ButtonDefaultForm urlCancelar='../index'/>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>

    )
}
