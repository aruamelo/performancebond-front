import React from 'react'
import { useParams } from "react-router-dom";
import frenteDeServicoService from "../../../../service/codificacaodedocumento/frentedeservico/FrenteDeServicoService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [frenteDeServico, setFrenteDeServico] = React.useState(null);

  React.useEffect(() => {
    frenteDeServicoService.showFrenteDeServico(id).then(data => {
      setFrenteDeServico(data.data)
    })
  }, [])

  if (!frenteDeServico) return null;

  const show = [
    { field: 'codigo', label: 'Código', value: frenteDeServico.codigo },
    { field: 'descricao', label: 'Descrição', value: frenteDeServico.descricao },
    { field: 'created_at', label: 'Data de criação', value: frenteDeServico.created_at }
  ]
  const breadcrumb =  {
    "id": "frentedeservico",
    "title": "Frente de servicço",
    "titlepage": "Informações da frente de serviço",
    "permission": "frente_de_servico__codificacao_de_documento__frente_de_servico"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='frentedeservico'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
