import React from 'react'
import { useParams } from "react-router-dom";
import tiposDeDocumentoService
  from "../../../../service/codificacaodedocumento/tiposdedocumento/TiposDeDocumentoService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [tipoDeDocumento, setTipoDeDocumento] = React.useState(null);

  React.useEffect(() => {
    tiposDeDocumentoService.showTipoDeDocumento(id).then(data => {
      setTipoDeDocumento(data.data)
    })
  }, [])

  if (!tipoDeDocumento) return null;

  const show = [
    { field: 'codigo', label: 'Código', value: tipoDeDocumento.codigo },
    { field: 'descricao', label: 'Descrição', value: tipoDeDocumento.descricao },
    { field: 'created_at', label: 'Data de criação', value: tipoDeDocumento.created_at }
  ]
  const breadcrumb =  {
    "id": "tiposdedocumento",
    "title": "Tipo de documento",
    "titlepage": "Informações do tipo de documento",
    "permission": "codificacao_de_documento__tipos_de_documento"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='tiposdedocumento'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
