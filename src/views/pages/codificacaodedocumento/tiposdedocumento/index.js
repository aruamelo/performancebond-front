import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import tiposDeDocumentoService
  from "../../../../service/codificacaodedocumento/tiposdedocumento/TiposDeDocumentoService";

export default function Index () {
  const [tiposDeDocumento, setTiposDeDocumento] = React.useState(null);

  React.useEffect(() => {
    tiposDeDocumentoService.tiposDeDocumento().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setTiposDeDocumento(data.data)
    })
  }, [])

  if (!tiposDeDocumento) return null;

  const columns = [
    { field: 'codigo', header: 'Código' },
    { field: 'descricao', header: 'Descrição' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={tiposDeDocumento}
                    urlDelete='codificacaodedocumento/tiposdedocumento/tiposdedocumento'/>
  )
}
