import React from 'react'
import { useParams } from "react-router-dom";
import nucleoService from "../../../../service/codificacaodedocumento/disciplina/NucleoService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewUsuarioDoSistema () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [nucleo, setNucleo] = React.useState(null);

  React.useEffect(() => {
    nucleoService.showNucleo(id).then(data => {
      setNucleo(data.data)
    })
  }, [])

  if (!nucleo) return null;

  const show = [
    { field: 'codigo', label: 'Código', value: nucleo.codigo },
    { field: 'descricao', label: 'Descrição', value: nucleo.descricao },
    { field: 'created_at', label: 'Data de criação', value: nucleo.created_at }
  ]
  const breadcrumb =  {
    "id": "nucleo",
    "title": "Núcleo",
    "titlepage": "Informações do núcleo",
    "permission": "codificacao_de_documento__nucleos"
  }
  return (
    <IndexView data={show} module='codificacaodedocumento' itemMenu='nucleo'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
