import React from 'react'
import { IndexDatatable } from '../../../../ui-component/index/indexDatatable'
import nucleoService from "../../../../service/codificacaodedocumento/disciplina/NucleoService";

export default function Index () {
  const [nucleo, setNucleo] = React.useState(null);

  React.useEffect(() => {
    nucleoService.nucleos().then(data => {
      data.data.map(item => {
        let textoSituacao = 'Ativo'
        if (!item.ativo) {
          textoSituacao = 'Inativo'
        }
        item.ativo = textoSituacao
      })
      setNucleo(data.data)
    })
  }, [])

  if (!nucleo) return null;

  const columns = [
    { field: 'codigo', header: 'Código' },
    { field: 'descricao', header: 'Descrição' },
    { field: 'ativo', header: 'Situação' },
  ]

  return (
    <IndexDatatable columns={columns} data={nucleo} urlDelete='codificacaodedocumento/nucleo/nucleos'/>
  )
}
