import React from 'react'
import { IconChevronRight } from '@tabler/icons-react'
import Container from 'react-bootstrap/Container';
import '../../../../assets/performancebond/css/perfomancebond.css'
import BreadcrumbsSubPage from "../../../../ui-component/extended/BreadcrumbsSubPage";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { useFormik } from 'formik';
import { classNames } from 'primereact/utils';
import nucleoService from "../../../../service/codificacaodedocumento/disciplina/NucleoService";
import noty from "../../../../alert/noty";
import distributedFunctions from "../../../../request/distributedFunctions";
import {ButtonDefaultForm} from "../../../../ui-component/button/ButtonDefaultForm";
import LoadingPerformancebond from "../../../../ui-component/loading/LoadingPerformancebond";

export default function Index () {
    const [loading, setLoading] = React.useState(false);

    const formik = useFormik({
        initialValues: {
            codigo: '',
            descricao: '',
            contrato_id: distributedFunctions.getIdContratoUser(),
            ativo: true
        },
        validate: (data) => {
            let errors = {};

            if (!data.descricao) {
                errors.descricao = 'Descrição do núcleo é obrigatório.';
            }

            if (!data.codigo) {
                errors.codigo = 'Código do núcleo é obrigatório.';
            }

            if (data.codigo.length > 5) {
                errors.codigo = 'Código do núcleo deve ser menor que cinco caracteres.';
            }

            if (data.descricao.length > 255) {
                errors.codigo = 'Descrição do núcleo deve ser menor que duzentos e cinquenta e cinco caracteres.';
            }

            return errors;
        },
        onSubmit: (data) => {
            setLoading(true)
            nucleoService.insertNucleo(data).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    distributedFunctions.redirectPage('index')
                }, 2000)
            }).catch((err) => {
                if (err.response.status === 401) {
                    window.location.href = '/pb'
                }
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const breadcrumb =  {
            "id": "nucleo",
            "title": "Núcleo",
            "titlepage": "Cadastrar núcleo",
            "permission": "codificacao_de_documento__nucleos"
    }

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />
            <form onSubmit={formik.handleSubmit}>
                <div className="card">
                    <Row key={'rowForm'}>
                        <Col xs={6} md={6} key={'colCodigo'}>
                            <Form.Label htmlFor="codigo">Código</Form.Label>
                            <Form.Control
                                type="text"
                                id="codigo"
                                name="codigo"
                                value={formik.values.codigo}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o código do núcleo"
                                onChange={formik.handleChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('codigo') })}
                            />
                            {getFormErrorMessage('codigo')}
                        </Col>
                        <Col xs={6} md={6} key={'colName'}>
                            <Form.Label htmlFor="name">Nome</Form.Label>
                            <Form.Control
                                type="text"
                                id="descricao"
                                name="descricao"
                                value={formik.values.descricao}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar o nome do núcleo"
                                onChange={formik.handleChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('descricao') })}
                            />
                            {getFormErrorMessage('descricao')}
                        </Col>
                    </Row>
                </div>

                <ButtonDefaultForm/>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>

    )
}
