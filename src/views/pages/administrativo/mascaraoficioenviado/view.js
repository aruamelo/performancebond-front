import React from 'react'
import { useParams } from "react-router-dom";
import mascaraOficioEnviadoService
  from "../../../../service/administrativo/mascaraoficioenviado/MascaraOficioEnviadoService";
import { IndexView } from '../../../../ui-component/index/indexView';
import '../../../../assets/performancebond/css/perfomancebond.css'
export default function ViewMascaraOficioEnviado () {

  const params = useParams();
  const id = params.id
  let pathNameUrl = window.location.pathname
  const [mascara, setMascara] = React.useState(null);

  React.useEffect(() => {
    mascaraOficioEnviadoService.showMascaraOficio(id).then(data => {
      setMascara(data.data)
    })
  }, [])

  if (!mascara) return null;

  const show = [
    { field: 'name', label: 'Nome', value: mascara.mascara },
    { field: 'created_at', label: 'Data de criação', value: mascara.created_at }
  ]
  const breadcrumb =  {
    "id": "mascaraoficioenviado",
    "title": "Ofício enviado",
    "titlepage": "Informações da máscara",
    "permission": "administrativo__mascara_oficio_enviado"
  }
  return (
    <IndexView data={show} module='permissoes' itemMenu='usuariodosistema'
               pathNameUrl={pathNameUrl} id={id} breadcrumb={breadcrumb}/>
  )
}
