import React from 'react'
import { IconChevronRight } from '@tabler/icons-react'
import Container from 'react-bootstrap/Container';
import '../../../../assets/performancebond/css/perfomancebond.css'
import BreadcrumbsSubPage from "../../../../ui-component/extended/BreadcrumbsSubPage";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { useFormik } from 'formik';
import { classNames } from 'primereact/utils';
import noty from "../../../../alert/noty";
import distributedFunctions from "../../../../request/distributedFunctions";
import {ButtonDefaultForm} from "../../../../ui-component/button/ButtonDefaultForm";
import mascaraOficioEnviadoService
    from "../../../../service/administrativo/mascaraoficioenviado/MascaraOficioEnviadoService";
import {useParams } from 'react-router-dom';
import LoadingPerformancebond from "../../../../ui-component/loading/LoadingPerformancebond";

export default function Edit () {
    const { id } = useParams();
    const [loading, setLoading] = React.useState(false);

    const formik = useFormik({
        initialValues: {
            mascara: ''
        },
        validate: (data) => {
            let errors = {};

            if (!data.mascara) {
                errors.mascara = 'Máscara é obrigatório.';
            }

            if (!data.mascara || !(/<numero>/.test(data.mascara) && /<ano>/.test(data.mascara))) {
                errors.mascara = 'A string deve conter as tags <numero> e <ano>.';
            }

            return errors;
        },
        onSubmit: (data) => {
            setLoading(true)
            data.contrato_id = distributedFunctions.getIdContratoUser()
            mascaraOficioEnviadoService.updateMascaraOficio(data).then(data => {
                noty.success(data.data.message)
                setTimeout(function () {
                    distributedFunctions.redirectPage('../index')
                }, 2000)
            }).catch((err) => {
                let mensagem = distributedFunctions.handleErrorMessage(err)

                noty.error(mensagem)
                setLoading(false)
            });
        }
    });

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        formik.setFieldValue(name, value);
    };

    const [dadosCarregados, setDadosCarregados] = React.useState(false);

    React.useEffect(() => {
        mascaraOficioEnviadoService.editMascaraOficio(id).then((data) => {
            // Configurar o estado inicial do formik com os dados do usuário
            formik.setValues({
                id: data.data.id,
                mascara: data.data.mascara
            });
            setDadosCarregados(true);
        });
    }, [id]);

    // Renderizar apenas quando os dados estiverem totalmente carregados
    if (!dadosCarregados) {
        return <p>Carregando...</p>;
    }

    const breadcrumb =  {
        "id": "mascaraoficioenviado",
        "title": "Ofício enviado",
        "titlepage": "Cadastrar máscara",
        "permission": "administrativo__mascara_oficio_enviado"
    }

    return (
        <Container fluid>
            <BreadcrumbsSubPage separator={IconChevronRight} navigation={breadcrumb} icon title rightAlign />
            <form onSubmit={formik.handleSubmit}>
                <div className="card">
                    <Row key={'rowForm'}>
                        <Col xs={6} md={4} key={'colName'}>
                            <Form.Label htmlFor="mascara">Nome</Form.Label>
                            <Form.Control
                                type="text"
                                id="mascara"
                                name="mascara"
                                value={formik.values.mascara}
                                aria-describedby="passwordHelpBlock"
                                placeholder ="Digitar a mascara para o ofício enviado"
                                onChange={handleInputChange}
                                className={classNames({ 'p-invalid': isFormFieldValid('mascara') })}
                            />
                            {getFormErrorMessage('mascara')}
                        </Col>
                    </Row>
                </div>

                <ButtonDefaultForm urlCancelar='../index'/>
            </form>
            <LoadingPerformancebond loading={loading}/>
        </Container>
    )
}
